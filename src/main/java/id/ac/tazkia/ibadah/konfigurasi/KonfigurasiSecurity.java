package id.ac.tazkia.ibadah.konfigurasi;

import id.ac.tazkia.ibadah.dao.config.UserDao;
import id.ac.tazkia.ibadah.entity.config.Permission;
import id.ac.tazkia.ibadah.entity.config.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper;
import org.springframework.security.oauth2.core.user.OAuth2UserAuthority;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.thymeleaf.extras.springsecurity6.dialect.SpringSecurityDialect;

import java.util.ArrayList;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.springframework.security.config.Customizer.withDefaults;

@Configuration
//@EnableGlobalMethodSecurity(prePostEnabled = true)
public class KonfigurasiSecurity {

    @Autowired
    private UserDao userDao;


    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests(auth -> auth
                .requestMatchers("/css/**", "/img/**", "/js/**").permitAll()
                .requestMatchers("/overview").hasAnyAuthority("KPI_MENU_OVERVIEW")
                .anyRequest().authenticated()
        )
        .logout().permitAll()
        .and().oauth2Login().loginPage("/signin").permitAll()
        .userInfoEndpoint()
        .userAuthoritiesMapper(authoritiesMapper())
        .and().defaultSuccessUrl("/", true);
        return http.build();
    }


    private GrantedAuthoritiesMapper authoritiesMapper(){
        return (authorities) -> {
            String emailAttrName = "email";
            String email = authorities.stream()
                    .filter(OAuth2UserAuthority.class::isInstance)
                    .map(OAuth2UserAuthority.class::cast)
                    .filter(userAuthority -> userAuthority.getAttributes().containsKey(emailAttrName))
                    .map(userAuthority -> userAuthority.getAttributes().get(emailAttrName).toString())
                    .findFirst()
                    .orElse(null);

            if (email == null) {
                return authorities;     // data email tidak ada di userInfo dari Google
            }

            User user = userDao.findByUsername(email);
            if(user == null) {
                throw new IllegalStateException("Email "+email+" tidak ada dalam database");
//                return null;
//                return authorities;     // email user ini belum terdaftar di database
            }

            Set<Permission> userAuthorities = ((User) user).getRole().getPermissions();
            if (userAuthorities.isEmpty()) {
                return authorities;     // authorities defaultnya ROLE_USER
            }

            return Stream.concat(
                    authorities.stream(),
                    userAuthorities.stream()
                            .map(Permission::getValue)
                            .map(SimpleGrantedAuthority::new)
            ).collect(Collectors.toCollection(ArrayList::new));
        };
    }

    @Bean
    public SpringSecurityDialect springSecurityDialect() {
        return new SpringSecurityDialect();
    }


    /*
    @Bean
    public WebSecurityCustomizer webSecurityCustomizer() {
        return (web) -> web.ignoring()
                .requestMatchers("/login")
                .requestMatchers("/css/**")
                .requestMatchers("/bootstrap52/**")
                .requestMatchers("/img/**")
                .requestMatchers("/**")
                .requestMatchers("/normal/**")
                .requestMatchers("/")
                .requestMatchers("/404")
                .requestMatchers("/images/**");
    }
*/




}
