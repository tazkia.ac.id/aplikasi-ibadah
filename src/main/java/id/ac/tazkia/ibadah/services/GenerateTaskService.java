package id.ac.tazkia.ibadah.services;

import id.ac.tazkia.ibadah.dao.DaysDao;
import id.ac.tazkia.ibadah.dao.attendance.AttendanceScheduleEventDao;
import id.ac.tazkia.ibadah.dao.masterdata.EmployesDao;
import id.ac.tazkia.ibadah.dao.schedule.ScheduleEventDao;
import id.ac.tazkia.ibadah.dao.setting.EmployeeTaskScheduleDao;
import id.ac.tazkia.ibadah.dao.setting.EmployeeTasksDao;
import id.ac.tazkia.ibadah.dao.setting.PayrollPeriodeDao;
import id.ac.tazkia.ibadah.dto.CariTasksDto;
import id.ac.tazkia.ibadah.dto.GenerateTanggalDto;
import id.ac.tazkia.ibadah.entity.Days;
import id.ac.tazkia.ibadah.entity.ScheduleType;
import id.ac.tazkia.ibadah.entity.StatusRecord;
import id.ac.tazkia.ibadah.entity.masterdata.Employes;
import id.ac.tazkia.ibadah.entity.schedule.ScheduleEvent;
import id.ac.tazkia.ibadah.entity.setting.EmployeeTasks;
import id.ac.tazkia.ibadah.entity.setting.EmployeeTasksSchedule;
import id.ac.tazkia.ibadah.entity.setting.PayrollPeriode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service
@EnableScheduling
public class GenerateTaskService {

    @Autowired
    private ScheduleEventDao scheduleEventDao;
    @Autowired
    private AttendanceScheduleEventDao attendanceScheduleEventDao;
    @Autowired
    private EmployesDao employesDao;
    @Autowired
    private DaysDao daysDao;

    @Autowired
    private EmployeeTasksDao employeeTasksDao;

    @Autowired
    private PayrollPeriodeDao payrollPeriodeDao;
    @Autowired
    private EmployeeTaskScheduleDao employeeTaskScheduleDao;


    @Scheduled(cron = "0 0 3 * * ?")
    @Async
    public void notifikasiScheduleEvent(){

        System.out.println("generate jadwal mulai");
        //Generate ScheduleEvent
        PayrollPeriode payrollPeriode = payrollPeriodeDao.findByStatus(StatusRecord.AKTIF);

        LocalDate tanggal = LocalDate.now();
        String bulanMulai = tanggal.getMonthValue()+"";

        if(bulanMulai.equals('1')){

        }
        if(bulanMulai.length() == 1 ){
            bulanMulai = "0" + bulanMulai;
        }
        String bulanSelesai = tanggal.plusMonths(1).getMonthValue()+"";
        if(bulanSelesai.length() == 1 ){
            bulanSelesai = "0" + bulanSelesai;
        }
        String tahunMulai = tanggal.getYear()+"";
        String tahunSelesai = tanggal.plusMonths(1).getYear()+"";

        String dayMulai = LocalDate.now().getDayOfMonth()+"";
        if(dayMulai.length() == 1 ){
            dayMulai = "0" + dayMulai;
        }

        String tanggalMulai = tahunMulai+'-'+bulanMulai+'-'+ dayMulai;
        String tanggalSelesai = tahunSelesai+'-'+bulanSelesai+'-'+payrollPeriode.getSampaiTanggal();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDateMulai = LocalDate.parse(tanggalMulai, formatter);
        LocalDate localDateSelesai = LocalDate.parse(tanggalSelesai, formatter);

        System.out.println("tanggal mulai = "+ localDateMulai);
        System.out.println("tanggal selesai = "+ localDateSelesai);

        List<GenerateTanggalDto> generateTanggalDtos = scheduleEventDao.generateTanggal(localDateMulai, localDateSelesai);

        List<EmployeeTasksSchedule> employeeTasksSchedules = employeeTaskScheduleDao.findByStatusAndDateTasksIsBetweenAndDateTasksIsAfter(StatusRecord.WAITING, localDateMulai.minusDays(1), localDateSelesai.plusDays(1), LocalDate.now().minusDays(1));

        if(employeeTasksSchedules != null){
            for (EmployeeTasksSchedule employeeTasksSchedule : employeeTasksSchedules){
                employeeTaskScheduleDao.delete(employeeTasksSchedule);
            }
        }

        if(generateTanggalDtos != null){
            for(GenerateTanggalDto generateTanggalDto : generateTanggalDtos){
                System.out.println("Tanggal : "+ generateTanggalDto.getGenDate());
                System.out.println("Hari : "+ generateTanggalDto.getFieldName());
                List<CariTasksDto> cariTasksDtos = new ArrayList<>();
                System.out.println(generateTanggalDto.getGenDate());
                System.out.println(generateTanggalDto.getNameDay());
                if(generateTanggalDto.getFieldName().equals("sunday")) {
                    cariTasksDtos = employeeTasksDao.cariTaskEmployeeSunday();
                }else if(generateTanggalDto.getFieldName().equals("monday")) {
                    cariTasksDtos = employeeTasksDao.cariTaskEmployeeMonday();
                }else if(generateTanggalDto.getFieldName().equals("tuesday")) {
                    cariTasksDtos = employeeTasksDao.cariTaskEmployeeTuesday();
                }else if(generateTanggalDto.getFieldName().equals("wednesday")) {
                    cariTasksDtos = employeeTasksDao.cariTaskEmployeeWednesday();
                }else if(generateTanggalDto.getFieldName().equals("thursday")) {
                    cariTasksDtos = employeeTasksDao.cariTaskEmployeeThursday();
                }else if(generateTanggalDto.getFieldName().equals("friday")) {
                    cariTasksDtos = employeeTasksDao.cariTaskEmployeeFriday();
                }else if(generateTanggalDto.getFieldName().equals("saturday")) {
                    cariTasksDtos = employeeTasksDao.cariTaskEmployeeSaturday();
                }

                System.out.println("Cari Task Dto : " + cariTasksDtos);
                if(cariTasksDtos != null){
                    for (CariTasksDto cariTasksDto : cariTasksDtos){
                        System.out.println("id : "+ cariTasksDto.getIdEmployee());
                        List<EmployeeTasksSchedule> employeeTasksSchedule1 = employeeTaskScheduleDao.findByStatusAndEmployeeTasksIdAndDateTasksOrderById(StatusRecord.DONE, cariTasksDto.getId(), generateTanggalDto.getGenDate());
                        if(employeeTasksSchedule1 == null || employeeTasksSchedule1.isEmpty()) {
                            EmployeeTasksSchedule employeeTasksSchedule = new EmployeeTasksSchedule();
                            employeeTasksSchedule.setEmployeeTasks(employeeTasksDao.findByStatusAndId(StatusRecord.AKTIF, cariTasksDto.getId()));
                            employeeTasksSchedule.setStatus(StatusRecord.WAITING);
                            employeeTasksSchedule.setDateTasks(generateTanggalDto.getGenDate());
                            employeeTaskScheduleDao.save(employeeTasksSchedule);
                        }
                    }
                }

                CariTasksDto cariTasksDto2 = employeeTasksDao.cariTaskEmployeeOnce(generateTanggalDto.getGenDate());
                if(cariTasksDto2 != null){
                    List<EmployeeTasksSchedule> employeeTasksSchedule1 = employeeTaskScheduleDao.findByStatusAndEmployeeTasksIdAndDateTasksOrderById(StatusRecord.DONE, cariTasksDto2.getId(), generateTanggalDto.getGenDate());
                    if(employeeTasksSchedule1 == null || employeeTasksSchedule1.isEmpty()) {
                        EmployeeTasksSchedule employeeTasksSchedule = new EmployeeTasksSchedule();
                        employeeTasksSchedule.setEmployeeTasks(employeeTasksDao.findByStatusAndId(StatusRecord.AKTIF, cariTasksDto2.getId()));
                        employeeTasksSchedule.setStatus(StatusRecord.WAITING);
                        employeeTasksSchedule.setDateTasks(generateTanggalDto.getGenDate());
                        employeeTaskScheduleDao.save(employeeTasksSchedule);
                    }
                }

//                List<ScheduleEvent> scheduleEvents = scheduleEventDao.findByStatusAndScheduleDateAndStatusAktifAndScheduleType(StatusRecord.AKTIF, generateTanggalDto.getGenDate(), StatusRecord.AKTIF, ScheduleType.Once);

//                List<EmployeeTasks> employeeTasks = employeeTasksDao.findByStatusAndScheduleEventScheduleDateAndScheduleEventStatusAndScheduleEventScheduleTypeAndScheduleEventJenis(StatusRecord.AKTIF, generateTanggalDto.getGenDate(), StatusRecord.AKTIF, ScheduleType.Once, StatusRecord.EVENT);
//                System.out.println("Employee Tasks : "+ employeeTasks);
//                if(employeeTasks != null){
//                    for(EmployeeTasks employeeTasks1 : employeeTasks){
//                        System.out.println("id : "+ employeeTasks1.getEmployes().getId());
//                        EmployeeTasksSchedule employeeTasksSchedule = new EmployeeTasksSchedule();
//                        employeeTasksSchedule.setEmployeeTasks(employeeTasksDao.findByStatusAndId(StatusRecord.AKTIF, employeeTasks1.getId()));
//                        employeeTasksSchedule.setStatus(StatusRecord.WAITING);
//                        employeeTasksSchedule.setDateTasks(generateTanggalDto.getGenDate());
//                        employeeTaskScheduleDao.save(employeeTasksSchedule);
//                    }
//                }
            }
        }

        System.out.println("generate jadwal selesai");
    }


    @Scheduled(cron = "0 0 11 * * ?")
    @Async
    public void notifikasiScheduleEvent2(){

        System.out.println("generate jadwal mulai");
        //Generate ScheduleEvent
        PayrollPeriode payrollPeriode = payrollPeriodeDao.findByStatus(StatusRecord.AKTIF);

        LocalDate tanggal = LocalDate.now();
        String bulanMulai = tanggal.getMonthValue()+"";

        if(bulanMulai.length() == 1 ){
            bulanMulai = "0" + bulanMulai;
        }
        String bulanSelesai = tanggal.plusMonths(1).getMonthValue()+"";
        if(bulanSelesai.length() == 1 ){
            bulanSelesai = "0" + bulanSelesai;
        }
        String tahunMulai = tanggal.getYear()+"";
        String tahunSelesai = tanggal.plusMonths(1).getYear()+"";

        String dayMulai = LocalDate.now().getDayOfMonth()+"";
        if(dayMulai.length() == 1 ){
            dayMulai = "0" + dayMulai;
        }

        String tanggalMulai = tahunMulai+'-'+bulanMulai+'-'+ dayMulai;
        String tanggalSelesai = tahunSelesai+'-'+bulanSelesai+'-'+payrollPeriode.getSampaiTanggal();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDateMulai = LocalDate.parse(tanggalMulai, formatter);
        LocalDate localDateSelesai = LocalDate.parse(tanggalSelesai, formatter);

        System.out.println("tanggal mulai = "+ localDateMulai);
        System.out.println("tanggal selesai = "+ localDateSelesai);

        List<GenerateTanggalDto> generateTanggalDtos = scheduleEventDao.generateTanggal(localDateMulai, localDateSelesai);

        List<EmployeeTasksSchedule> employeeTasksSchedules = employeeTaskScheduleDao.findByStatusAndDateTasksIsBetweenAndDateTasksIsAfter(StatusRecord.WAITING, localDateMulai.minusDays(1), localDateSelesai.plusDays(1), LocalDate.now().minusDays(1));

        if(employeeTasksSchedules != null){
            for (EmployeeTasksSchedule employeeTasksSchedule : employeeTasksSchedules){
                employeeTaskScheduleDao.delete(employeeTasksSchedule);
            }
        }

        if(generateTanggalDtos != null){
            for(GenerateTanggalDto generateTanggalDto : generateTanggalDtos){
                System.out.println("Tanggal : "+ generateTanggalDto.getGenDate());
                System.out.println("Hari : "+ generateTanggalDto.getFieldName());
                List<CariTasksDto> cariTasksDtos = new ArrayList<>();
                System.out.println(generateTanggalDto.getGenDate());
                System.out.println(generateTanggalDto.getNameDay());
                if(generateTanggalDto.getFieldName().equals("sunday")) {
                    cariTasksDtos = employeeTasksDao.cariTaskEmployeeSunday();
                }else if(generateTanggalDto.getFieldName().equals("monday")) {
                    cariTasksDtos = employeeTasksDao.cariTaskEmployeeMonday();
                }else if(generateTanggalDto.getFieldName().equals("tuesday")) {
                    cariTasksDtos = employeeTasksDao.cariTaskEmployeeTuesday();
                }else if(generateTanggalDto.getFieldName().equals("wednesday")) {
                    cariTasksDtos = employeeTasksDao.cariTaskEmployeeWednesday();
                }else if(generateTanggalDto.getFieldName().equals("thursday")) {
                    cariTasksDtos = employeeTasksDao.cariTaskEmployeeThursday();
                }else if(generateTanggalDto.getFieldName().equals("friday")) {
                    cariTasksDtos = employeeTasksDao.cariTaskEmployeeFriday();
                }else if(generateTanggalDto.getFieldName().equals("saturday")) {
                    cariTasksDtos = employeeTasksDao.cariTaskEmployeeSaturday();
                }

                System.out.println("Cari Task Dto : " + cariTasksDtos);
                if(cariTasksDtos != null){
                    for (CariTasksDto cariTasksDto : cariTasksDtos){
                        System.out.println("id : "+ cariTasksDto.getIdEmployee());
                        List<EmployeeTasksSchedule> employeeTasksSchedule1 = employeeTaskScheduleDao.findByStatusAndEmployeeTasksIdAndDateTasksOrderById(StatusRecord.DONE, cariTasksDto.getId(), generateTanggalDto.getGenDate());
                        if(employeeTasksSchedule1 == null || employeeTasksSchedule1.isEmpty()) {
                            EmployeeTasksSchedule employeeTasksSchedule = new EmployeeTasksSchedule();
                            employeeTasksSchedule.setEmployeeTasks(employeeTasksDao.findByStatusAndId(StatusRecord.AKTIF, cariTasksDto.getId()));
                            employeeTasksSchedule.setStatus(StatusRecord.WAITING);
                            employeeTasksSchedule.setDateTasks(generateTanggalDto.getGenDate());
                            employeeTaskScheduleDao.save(employeeTasksSchedule);
                        }
                    }
                }

                CariTasksDto cariTasksDto2 = employeeTasksDao.cariTaskEmployeeOnce(generateTanggalDto.getGenDate());
                if(cariTasksDto2 != null){
                    List<EmployeeTasksSchedule> employeeTasksSchedule1 = employeeTaskScheduleDao.findByStatusAndEmployeeTasksIdAndDateTasksOrderById(StatusRecord.DONE, cariTasksDto2.getId(), generateTanggalDto.getGenDate());
                    if(employeeTasksSchedule1 == null || employeeTasksSchedule1.isEmpty()) {
                        EmployeeTasksSchedule employeeTasksSchedule = new EmployeeTasksSchedule();
                        employeeTasksSchedule.setEmployeeTasks(employeeTasksDao.findByStatusAndId(StatusRecord.AKTIF, cariTasksDto2.getId()));
                        employeeTasksSchedule.setStatus(StatusRecord.WAITING);
                        employeeTasksSchedule.setDateTasks(generateTanggalDto.getGenDate());
                        employeeTaskScheduleDao.save(employeeTasksSchedule);
                    }
                }

//                List<ScheduleEvent> scheduleEvents = scheduleEventDao.findByStatusAndScheduleDateAndStatusAktifAndScheduleType(StatusRecord.AKTIF, generateTanggalDto.getGenDate(), StatusRecord.AKTIF, ScheduleType.Once);
//
//                List<EmployeeTasks> employeeTasks = employeeTasksDao.findByStatusAndScheduleEventScheduleDateAndScheduleEventStatusAndScheduleEventScheduleTypeAndScheduleEventJenis(StatusRecord.AKTIF, generateTanggalDto.getGenDate(), StatusRecord.AKTIF, ScheduleType.Once, StatusRecord.EVENT);
//                System.out.println("Employee Tasks : "+ employeeTasks);
//                if(employeeTasks != null){
//                    for(EmployeeTasks employeeTasks1 : employeeTasks){
//                        System.out.println("id : "+ employeeTasks1.getEmployes().getId());
//                        EmployeeTasksSchedule employeeTasksSchedule = new EmployeeTasksSchedule();
//                        employeeTasksSchedule.setEmployeeTasks(employeeTasksDao.findByStatusAndId(StatusRecord.AKTIF, employeeTasks1.getId()));
//                        employeeTasksSchedule.setStatus(StatusRecord.WAITING);
//                        employeeTasksSchedule.setDateTasks(generateTanggalDto.getGenDate());
//                        employeeTaskScheduleDao.save(employeeTasksSchedule);
//                    }
//                }
            }
        }

        System.out.println("generate jadwal selesai");
    }


}
