package id.ac.tazkia.ibadah.dto;

import java.time.LocalDate;

public interface GenerateTanggalDto {

    LocalDate getGenDate();
    Integer getHari();
    String getNameDay();
    String getFieldName();

}
