package id.ac.tazkia.ibadah.dto;

import java.time.LocalDate;

public interface TaskScheduleEmployeeReportsDto {

    String getClas();
    String getScheduleName();
    LocalDate getDateTasks();
    String getId();

}
