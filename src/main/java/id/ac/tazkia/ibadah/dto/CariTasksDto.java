package id.ac.tazkia.ibadah.dto;

import java.math.BigDecimal;

public interface CariTasksDto {

    String getId();
    String getIdEmployee();
    String getIdScheduleEvent();
    BigDecimal getReward();

}
