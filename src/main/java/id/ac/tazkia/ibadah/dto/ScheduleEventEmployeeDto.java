package id.ac.tazkia.ibadah.dto;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;

public interface ScheduleEventEmployeeDto {

    String getId();
    LocalDate getScheduleDate();
    LocalTime getScheduleTimeStart();
    LocalTime getScheduleTimeEnd();
    String getScheduleName();
    String getScheduleDescription();
    String getScheduleType();
    String getUploadPicture();
    String getMustMatch();
    BigDecimal getReward();
    String getJenis();
    String getStatusAbsen();
    String getAbsenDescription();

}
