package id.ac.tazkia.ibadah.dto;

import java.math.BigDecimal;

public interface PerformanceReportDto {

    String getId();
    String getFullName();
    String getScheduleName();
    BigDecimal getJumlah();
    BigDecimal getTarif();
    BigDecimal getReward();

}
