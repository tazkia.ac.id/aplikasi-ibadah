package id.ac.tazkia.ibadah.entity.attendance;

import id.ac.tazkia.ibadah.entity.StatusRecord;
import id.ac.tazkia.ibadah.entity.masterdata.Employes;
import id.ac.tazkia.ibadah.entity.schedule.ScheduleEvent;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@Data
public class AttendanceScheduleEvent {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_schedule_event")
    private ScheduleEvent scheduleEvent;

    @ManyToOne
    @JoinColumn(name = "id_employee")
    private Employes employes;

    private LocalDate dateAttendance;

    private LocalTime timeIn;

    private LocalDateTime DateTimeIn;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userInsert;

    private String userUpdate;

    private String userDelete;

    private LocalDateTime dateInsert;

    private LocalDateTime dateUpdate;

    private LocalDateTime dateDelete;

    private String description;

    private String fileUpload;

    @Enumerated(EnumType.STRING)
    private StatusRecord attendanceStatus;

    @Enumerated(EnumType.STRING)
    private StatusRecord eventType;

    private LocalDate scheduleDate;

    private LocalTime scheculeTimeStart;

    private LocalTime scheduleTimeEnd;

}
