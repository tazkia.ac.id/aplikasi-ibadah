package id.ac.tazkia.ibadah.entity.masterdata;

import id.ac.tazkia.ibadah.entity.StatusRecord;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.jetbrains.annotations.NotNull;

import java.time.LocalDateTime;

@Entity
@Data
public class Department {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    private String departmentCode;

    @NotNull
    private String departmentName;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userUpdate;

    private LocalDateTime dateUpdate;

    @ManyToOne
    @JoinColumn(name = "id_company")
    private Companies companies;

}
