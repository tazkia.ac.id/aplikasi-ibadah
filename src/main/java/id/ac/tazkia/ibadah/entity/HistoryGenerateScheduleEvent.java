package id.ac.tazkia.ibadah.entity;

import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Data
public class HistoryGenerateScheduleEvent {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private LocalDate dateGenerate;

    private LocalTime timeStart;

    private LocalTime timeEnd;

    @Enumerated(EnumType.STRING)
    private StatusRecord type;
    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

}
