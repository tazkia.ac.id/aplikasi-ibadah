package id.ac.tazkia.ibadah.entity.setting;

import id.ac.tazkia.ibadah.entity.StatusRecord;
import jakarta.persistence.*;
import jdk.jfr.Enabled;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Data
public class TasksScore {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private BigDecimal gradeAa;

    private BigDecimal gradeBe;

    private BigDecimal gradeCe;

    private BigDecimal gradeDe;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userUpdate;

    private LocalDateTime dateUpdate;

}
