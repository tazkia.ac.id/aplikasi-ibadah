package id.ac.tazkia.ibadah.entity.setting;

import id.ac.tazkia.ibadah.entity.StatusRecord;
import id.ac.tazkia.ibadah.entity.masterdata.Employes;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Data
public class EmployeeTasksSchedule {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_employee_tasks")
    private EmployeeTasks employeeTasks;

    private LocalDate dateTasks;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String description;

    private String fileUpload;

    private String fileName;

    private BigDecimal reward;

    private LocalDateTime dateLog;

    @ManyToOne
    @JoinColumn(name = "employee_log")
    private Employes employes;



}
