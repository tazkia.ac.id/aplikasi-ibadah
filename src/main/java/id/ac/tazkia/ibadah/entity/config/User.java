package id.ac.tazkia.ibadah.entity.config;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
@Entity
@Table(name = "s_user")
//@Data
@Getter
@Setter
@NoArgsConstructor
//@EqualsAndHashCode(of = "id")
public class User {

    @Id
//    @GeneratedValue(generator = "uuid" )
//    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
    private String username;
    private Boolean active;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_role")
    private Role role;

//    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL, optional = false)
//    private Employes employes = new Employes();

//    @OneToOne(fetch = FetchType.LAZY,
//            cascade =  CascadeType.ALL,
//            mappedBy = "user")
//    private Employes employes;

//    @OneToMany(mappedBy = "attendanceRequest", cascade = CascadeType.ALL, orphanRemoval = true)
//    private Set<AttendanceRequestApproval> attendanceRequestApproval = new HashSet<>();

}
