package id.ac.tazkia.ibadah.entity;

public enum ScheduleType {

    Daily, Custom, Once

}
