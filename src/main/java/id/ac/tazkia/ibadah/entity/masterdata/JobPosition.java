package id.ac.tazkia.ibadah.entity.masterdata;

import id.ac.tazkia.ibadah.entity.StatusRecord;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.jetbrains.annotations.NotNull;

import java.time.LocalDateTime;

@Entity
@Data
public class JobPosition {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_department")
    private Department department;

    @NotNull
    private String positionCode;

    @NotNull
    private String positionName;

    @NotNull
    private String description;

    private String file;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userUpdate;

    private LocalDateTime dateUpdate;

    @Enumerated(EnumType.STRING)
    private StatusRecord overtimeEligible = StatusRecord.AKTIF;

}
