package id.ac.tazkia.ibadah.entity.masterdata;

import id.ac.tazkia.ibadah.entity.EmployeeClass;
import id.ac.tazkia.ibadah.entity.StatusRecord;
import id.ac.tazkia.ibadah.entity.config.User;
import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDate;

@Entity
@Data
public class Employes {

    @Id
    private String id;

    private String number;

    private String nickName;

    private String fullName;

    //    @NotNull
    private String gender;

    @OneToOne
    @JoinColumn(name = "id_user")
    private User user;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    @Enumerated(EnumType.STRING)
    private StatusRecord statusAktif;

    @Enumerated(EnumType.STRING)
    private EmployeeClass employeeClass;

    @Enumerated(EnumType.STRING)
    private StatusRecord jobStatus = StatusRecord.AKTIF;

    @ManyToOne
    @JoinColumn(name = "id_company")
    private Companies companies;

}
