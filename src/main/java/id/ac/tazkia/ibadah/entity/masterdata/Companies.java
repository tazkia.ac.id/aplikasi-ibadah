package id.ac.tazkia.ibadah.entity.masterdata;


import id.ac.tazkia.ibadah.entity.StatusRecord;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class Companies {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    private String companyName;

    @NotNull
    private String companyType;

    @NotNull
    private String regNumber;

    @NotNull
    private String address;

    @NotNull
    private String regency;

    @NotNull
    private String country;

    @NotNull
    private String postalCode;

    private String description;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userUpdate;

    private LocalDateTime dateUpdate;

//    @ManyToOne
//    @JoinColumn(name = "id_main_company")
//    private Companies mainCompany;

}
