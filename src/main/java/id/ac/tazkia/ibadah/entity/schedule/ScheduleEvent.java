package id.ac.tazkia.ibadah.entity.schedule;

import id.ac.tazkia.ibadah.entity.Days;
import id.ac.tazkia.ibadah.entity.EmployeeClass;
import id.ac.tazkia.ibadah.entity.ScheduleType;
import id.ac.tazkia.ibadah.entity.StatusRecord;
import id.ac.tazkia.ibadah.entity.masterdata.Companies;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@Data
public class ScheduleEvent {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_company")
    private Companies companies;

    private LocalDate scheduleDate;

    private LocalTime scheduleTimeStart;

    private LocalTime scheduleTimeEnd;

    private String scheduleName;

    private String scheduleDescription;

    @Enumerated(EnumType.STRING)
    private StatusRecord statusAktif = StatusRecord.AKTIF;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private LocalDateTime dateInsert;

    private LocalDateTime dateUpdate;

    private LocalDateTime dateDelete;

    private String userInsert;

    private String userUpdate;

    private String userDelete;

    private String scheduleDateString;

    private String scheduleTimeStartString;

    private String scheduleTimeEndString;

    @Enumerated(EnumType.STRING)
    private ScheduleType scheduleType;

    @Enumerated(EnumType.STRING)
    private StatusRecord uploadPicture;

    @Enumerated(EnumType.STRING)
    private StatusRecord mustMatch;

    private BigDecimal reward;

    private String task;

    @Enumerated(EnumType.STRING)
    private StatusRecord jenis;

    @Enumerated(EnumType.STRING)
    private StatusRecord sunday;

    @Enumerated(EnumType.STRING)
    private StatusRecord monday;

    @Enumerated(EnumType.STRING)
    private StatusRecord tuesday;

    @Enumerated(EnumType.STRING)
    private StatusRecord wednesday;

    @Enumerated(EnumType.STRING)
    private StatusRecord thursday;

    @Enumerated(EnumType.STRING)
    private StatusRecord friday;

    @Enumerated(EnumType.STRING)
    private StatusRecord saturday;

    @Enumerated(EnumType.STRING)
    private EmployeeClass employeeClass;

}
