package id.ac.tazkia.ibadah.entity.setting;

import id.ac.tazkia.ibadah.entity.StatusRecord;
import id.ac.tazkia.ibadah.entity.masterdata.Employes;
import id.ac.tazkia.ibadah.entity.schedule.ScheduleEvent;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import java.time.LocalDateTime;

@Entity
@Data
public class EmployeeTasks {

    @Id
//    @GeneratedValue(generator = "uuid" )
//    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_employee")
    private Employes employes;

    @ManyToOne
    @JoinColumn(name = "id_schedule_event")
    private ScheduleEvent scheduleEvent;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String userInsert;

    private String userEdit;

    private String userDelete;

    private LocalDateTime tanggalInsert;

    private LocalDateTime tanggalEdit;

    private LocalDateTime tanggalDelete;

}
