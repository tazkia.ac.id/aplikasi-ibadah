package id.ac.tazkia.ibadah.dao.setting;

import id.ac.tazkia.ibadah.entity.StatusRecord;
import id.ac.tazkia.ibadah.entity.masterdata.Employes;
import id.ac.tazkia.ibadah.entity.setting.EmployeeJobPosition;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface EmployeeJobPositionDao extends PagingAndSortingRepository<EmployeeJobPosition, String> {

    @Query(value = "select a.id, a.full_name, group_concat(concat('-',b.position_name) separator '\\n') as position_name from\n" +
            "(select * from employes where status = 'AKTIF' and status_aktif = 'AKTIF') as a\n" +
            "left join\n" +
            "(select id_employee, position_name from employee_job_position as a\n" +
            "inner join job_position as b on a.id_position = b.id \n" +
            "where start_date <= date(now()) and end_date >= date(now()) and a.status = 'AKTIF' and a.status_aktif = 'AKTIF' group by id_employee, position_name) as b\n" +
            "on a.id = b.id_employee group by id_employee order by full_name", countQuery = "select count(a.id) as id from\n" +
            "(select a.id, a.full_name, group_concat(concat('-',b.position_name) separator '\\n') as position_name from\n" +
            "(select * from employes where status = 'AKTIF' and status_aktif = 'AKTIF') as a\n" +
            "left join\n" +
            "(select id_employee, position_name from employee_job_position as a\n" +
            "inner join job_position as b on a.id_position = b.id \n" +
            "where start_date <= date(now()) and end_date >= date(now()) and a.status = 'AKTIF' and a.status_aktif = 'AKTIF' group by id_employee, position_name) as b\n" +
            "on a.id = b.id_employee group by id_employee order by full_name) as a", nativeQuery = true)
    Page<Object> listEmployeeJobPositiob(Pageable pageable);


    @Query(value = "SELECT a.id, a.full_name, COALESCE(c.jabatan, '') AS jabatan, employee_class FROM employes AS a \n" +
            "INNER JOIN employee_schedule AS b ON a.id = b.id_employee \n" +
            "LEFT JOIN (SELECT a.id_employee, GROUP_CONCAT(b.position_name SEPARATOR '\n') AS jabatan \n" +
            "FROM employee_job_position AS a " +
            "INNER JOIN job_position AS b ON a.id_position = b.id \n" +
            "WHERE a.status = 'AKTIF' AND b.status = 'AKTIF' GROUP BY a.id_employee) AS c \n" +
            "ON a.id = c.id_employee \n" +
            "WHERE a.status = 'AKTIF' AND b.status = 'AKTIF' AND a.status_aktif = 'AKTIF' \n" +
            "GROUP BY a.id ORDER BY a.full_name", countQuery = "select count(id) as jml from (select a.id, a.full_name,jabatan, employee_class from employes as a\n" +
            "inner join employee_schedule as b on a.id = b.id_employee\n" +
            "left join (select a.id_employee,group_concat(b.position_name Separator \" -\") as jabatan from employee_job_position as a inner join job_position as b on a.id_position = b.id \n" +
            "where a.status = 'AKTIF' and b.status = 'AKTIF' group by a.id_employee) as c on a.id = c.id_employee\n" +
            "where a.status = 'AKTIF' and b.status = 'AKTIF' and a.status_aktif = 'AKTIF' group by a.id order by full_name)as a",nativeQuery = true)
    Page<Object[]> cariNamaKrywanaScheduleTasks(Pageable pageable);


}
