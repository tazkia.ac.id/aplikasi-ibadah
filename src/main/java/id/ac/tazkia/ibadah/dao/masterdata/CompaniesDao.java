package id.ac.tazkia.ibadah.dao.masterdata;

import id.ac.tazkia.ibadah.entity.StatusRecord;
import id.ac.tazkia.ibadah.entity.masterdata.Companies;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CompaniesDao extends PagingAndSortingRepository<Companies, String> {

    Companies findByStatusAndId(StatusRecord statusRecord, String id);

}
