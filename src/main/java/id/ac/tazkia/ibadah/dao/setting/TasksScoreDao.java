package id.ac.tazkia.ibadah.dao.setting;

import id.ac.tazkia.ibadah.entity.StatusRecord;
import id.ac.tazkia.ibadah.entity.setting.TasksScore;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface TasksScoreDao extends PagingAndSortingRepository<TasksScore, String>, CrudRepository<TasksScore, String> {

    TasksScore findTopByStatusOrderByDateUpdateDesc(StatusRecord statusRecord);

}
