package id.ac.tazkia.ibadah.dao.setting;

import id.ac.tazkia.ibadah.dto.CariTasksDto;
import id.ac.tazkia.ibadah.entity.ScheduleType;
import id.ac.tazkia.ibadah.entity.StatusRecord;
import id.ac.tazkia.ibadah.entity.masterdata.Employes;
import id.ac.tazkia.ibadah.entity.schedule.ScheduleEvent;
import id.ac.tazkia.ibadah.entity.setting.EmployeeTasks;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface EmployeeTasksDao extends CrudRepository<EmployeeTasks, String>,PagingAndSortingRepository<EmployeeTasks, String> {

    Page<EmployeeTasks> findByStatusAndEmployes(StatusRecord statusRecord, Employes employes, Pageable pageable);

    EmployeeTasks findByStatusAndEmployesAndScheduleEvent(StatusRecord statusRecord, Employes employes, ScheduleEvent scheduleEvent);

    Page<EmployeeTasks> findByStatusAndEmployesAndScheduleEventTaskContainingIgnoreCase(StatusRecord statusRecord, Employes employes, String task, Pageable pageable);


    EmployeeTasks findByStatusAndId(StatusRecord statusRecord, String id);

    List<EmployeeTasks> findByStatusAndScheduleEventScheduleDateAndScheduleEventStatusAndScheduleEventScheduleTypeAndScheduleEventJenis(StatusRecord statusRecord, LocalDate localDate, StatusRecord statusRecord3, ScheduleType scheduleType, StatusRecord statusRecord4);

    @Query(value = "select a.id, id_employee as idEmployee,id_schedule_event as idScheduleEvent, reward from employee_tasks as a \n" +
            "inner join schedule_event as b on a.id_schedule_event = b.id \n" +
            "where a.status = 'AKTIF' and b.status = 'AKTIF' and (:column1 = 'AKTIF') and b.schedule_type in ('Custom','Daily')", nativeQuery = true)
    List<CariTasksDto> cariTaskEmployee(@Param("column1") String column1);

    @Query(value = "select a.id, id_employee as idEmployee,id_schedule_event as idScheduleEvent, reward from employee_tasks as a \n" +
            "inner join schedule_event as b on a.id_schedule_event = b.id \n" +
            "where a.status = 'AKTIF' and b.status = 'AKTIF' and sunday = 'AKTIF' and b.schedule_type in ('Custom','Daily')", nativeQuery = true)
    List<CariTasksDto> cariTaskEmployeeSunday();

    @Query(value = "select a.id, id_employee as idEmployee,id_schedule_event as idScheduleEvent, reward from employee_tasks as a \n" +
            "inner join schedule_event as b on a.id_schedule_event = b.id \n" +
            "where a.status = 'AKTIF' and b.status = 'AKTIF' and monday = 'AKTIF' and b.schedule_type in ('Custom','Daily')", nativeQuery = true)
    List<CariTasksDto> cariTaskEmployeeMonday();

    @Query(value = "select a.id, id_employee as idEmployee,id_schedule_event as idScheduleEvent, reward from employee_tasks as a \n" +
            "inner join schedule_event as b on a.id_schedule_event = b.id \n" +
            "where a.status = 'AKTIF' and b.status = 'AKTIF' and tuesday = 'AKTIF' and b.schedule_type in ('Custom','Daily')", nativeQuery = true)
    List<CariTasksDto> cariTaskEmployeeTuesday();

    @Query(value = "select a.id, id_employee as idEmployee,id_schedule_event as idScheduleEvent, reward from employee_tasks as a \n" +
            "inner join schedule_event as b on a.id_schedule_event = b.id \n" +
            "where a.status = 'AKTIF' and b.status = 'AKTIF' and wednesday = 'AKTIF' and b.schedule_type in ('Custom','Daily')", nativeQuery = true)
    List<CariTasksDto> cariTaskEmployeeWednesday();

    @Query(value = "select a.id, id_employee as idEmployee,id_schedule_event as idScheduleEvent, reward from employee_tasks as a \n" +
            "inner join schedule_event as b on a.id_schedule_event = b.id \n" +
            "where a.status = 'AKTIF' and b.status = 'AKTIF' and thursday = 'AKTIF' and b.schedule_type in ('Custom','Daily')", nativeQuery = true)
    List<CariTasksDto> cariTaskEmployeeThursday();

    @Query(value = "select a.id, id_employee as idEmployee,id_schedule_event as idScheduleEvent, reward from employee_tasks as a \n" +
            "inner join schedule_event as b on a.id_schedule_event = b.id \n" +
            "where a.status = 'AKTIF' and b.status = 'AKTIF' and friday = 'AKTIF' and b.schedule_type in ('Custom','Daily')", nativeQuery = true)
    List<CariTasksDto> cariTaskEmployeeFriday();

    @Query(value = "select a.id, id_employee as idEmployee,id_schedule_event as idScheduleEvent, reward from employee_tasks as a \n" +
            "inner join schedule_event as b on a.id_schedule_event = b.id \n" +
            "where a.status = 'AKTIF' and b.status = 'AKTIF' and saturday = 'AKTIF' and b.schedule_type in ('Custom','Daily')", nativeQuery = true)
    List<CariTasksDto> cariTaskEmployeeSaturday();

    @Query(value = "select a.id, id_employee as idEmployee,id_schedule_event as idScheduleEvent, reward from employee_tasks as a \n" +
            "inner join schedule_event as b on a.id_schedule_event = b.id \n" +
            "where a.status = 'AKTIF' and b.status = 'AKTIF' and saturday = 'AKTIF' and b.jenis = 'TASKS' and schedule_date = ?1 and b.schedule_type in ('Once')", nativeQuery = true)
    CariTasksDto cariTaskEmployeeOnce(LocalDate tanggal);


}
