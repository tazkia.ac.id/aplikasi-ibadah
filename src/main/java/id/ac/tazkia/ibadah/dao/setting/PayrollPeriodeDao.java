package id.ac.tazkia.ibadah.dao.setting;


import id.ac.tazkia.ibadah.entity.StatusRecord;
import id.ac.tazkia.ibadah.entity.setting.PayrollPeriode;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PayrollPeriodeDao extends PagingAndSortingRepository<PayrollPeriode, String> {

    PayrollPeriode findByStatus(StatusRecord statusRecord);

}
