package id.ac.tazkia.ibadah.dao.config;

import id.ac.tazkia.ibadah.entity.config.Role;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RoleDao extends PagingAndSortingRepository<Role, String> {


}
