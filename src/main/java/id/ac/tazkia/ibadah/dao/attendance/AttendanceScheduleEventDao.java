package id.ac.tazkia.ibadah.dao.attendance;

import id.ac.tazkia.ibadah.entity.StatusRecord;
import id.ac.tazkia.ibadah.entity.attendance.AttendanceScheduleEvent;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.time.LocalDate;
import java.util.List;

public interface AttendanceScheduleEventDao extends CrudRepository<AttendanceScheduleEvent, String>, PagingAndSortingRepository<AttendanceScheduleEvent, String> {

    List<AttendanceScheduleEvent> findByStatusAndEventTypeNotAndScheduleDateOrderByScheculeTimeStart(StatusRecord statusRecord, StatusRecord eventType, LocalDate tanggalSchedule);

}
