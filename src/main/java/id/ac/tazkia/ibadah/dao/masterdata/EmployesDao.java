package id.ac.tazkia.ibadah.dao.masterdata;

import id.ac.tazkia.ibadah.entity.EmployeeClass;
import id.ac.tazkia.ibadah.entity.StatusRecord;
import id.ac.tazkia.ibadah.entity.config.User;
import id.ac.tazkia.ibadah.entity.masterdata.Employes;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface EmployesDao extends PagingAndSortingRepository<Employes, String>, CrudRepository<Employes, String> {

    Page<Employes> findByStatusAndStatusAktifOrderByFullName(StatusRecord statusRecord, String statusRecord2, Pageable page);

    Employes findByStatusAndId(StatusRecord statusRecord, String id);

    Employes findByUser(User user);

    List<Employes> findByStatusAndStatusAktif(StatusRecord statusRecor, StatusRecord statusRecord);

    Page<Employes> findByStatusAndStatusAktifOrderByFullName(StatusRecord statusRecord, StatusRecord statusRecord2, Pageable pageable);

    Page<Employes> findByStatusAndStatusAktifAndFullNameContainingIgnoreCaseOrderByFullName(StatusRecord statusRecord, StatusRecord statusRecord2, String search, Pageable pageable);

    Page<Employes> findByStatusAndStatusAktifAndEmployeeClassOrderByFullName(StatusRecord statusRecord, StatusRecord statusRecord2, EmployeeClass employeeClass, Pageable pageable);

    Page<Employes> findByStatusAndStatusAktifAndEmployeeClassAndFullNameContainingIgnoreCaseOrderByFullName(StatusRecord statusRecord, StatusRecord statusRecord2, EmployeeClass employeeClass, String search, Pageable pageable);


    @Query(value = "select a.id,full_name,a.employee_class, group_concat('-', c.schedule_name order by schedule_name separator '\\n') as schedule, a.job_status as status, z.company_name as company   from employes as a inner join companies as z on a.id_company = z.id\n" +
            "left join (select * from employee_tasks where status = 'AKTIF') as b on a.id = b.id_employee\n" +
            "left join (select * from schedule_event where status = 'AKTIF') as c on b.id_schedule_event = c.id\n" +
            "where a.status = 'AKTIF' and a.status_aktif = 'AKTIF' and coalesce(b.status,'AKTIF') = 'AKTIF' and coalesce(c.jenis,'TASK') = 'TASK' group by a.id order by a.full_name;\n",
            countQuery = "select count(id) as jumlah from employes where status = 'AKTIF' and status_aktif = 'AKTIF'", nativeQuery = true)
    Page<Object> listKaryawanSchedule(Pageable pageable);

    @Query(value = "select a.id,full_name,a.employee_class, group_concat('-', c.schedule_name order by schedule_name separator '\\n') as schedule, a.job_status as status, z.company_name as company   from employes as a inner join companies as z on a.id_company = z.id \n" +
            "left join (select * from employee_tasks where status = 'AKTIF') as b on a.id = b.id_employee\n" +
            "left join (select * from schedule_event where status = 'AKTIF')as c on b.id_schedule_event = c.id\n" +
            "where a.status = 'AKTIF' and a.status_aktif = 'AKTIF' and a.full_name like %?1% and coalesce(b.status,'AKTIF') = 'AKTIF' and coalesce(c.jenis,'TASK') = 'TASK' group by a.id order by a.full_name",
            countQuery = "select count(id) as jumlah from employes where status = 'AKTIF' and status_aktif = 'AKTIF' and full_name like %?1%", nativeQuery = true)
    Page<Object> listKaryawanScheduleSearch(String search, Pageable pageable);

}
