package id.ac.tazkia.ibadah.dao.setting;


import id.ac.tazkia.ibadah.dto.PerformanceReportDto;
import id.ac.tazkia.ibadah.dto.TaskScheduleEmployeeReportsDto;
import id.ac.tazkia.ibadah.entity.StatusRecord;
import id.ac.tazkia.ibadah.entity.masterdata.Employes;
import id.ac.tazkia.ibadah.entity.setting.EmployeeTasks;
import id.ac.tazkia.ibadah.entity.setting.EmployeeTasksSchedule;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

public interface EmployeeTaskScheduleDao extends CrudRepository<EmployeeTasksSchedule, String>, PagingAndSortingRepository<EmployeeTasksSchedule, String> {

    EmployeeTasksSchedule findByEmployeeTasksAndStatus(EmployeeTasks employeeTasks, StatusRecord statusRecord);

    List<EmployeeTasksSchedule> findByStatusAndDateTasksIsBetweenAndDateTasksIsAfter(StatusRecord statusRecord, LocalDate tanggalMulai, LocalDate tanggalSelesai, LocalDate tanggalLewat);

    EmployeeTasksSchedule findByEmployeeTasksAndStatusNot(EmployeeTasks employeeTasks, StatusRecord statusRecord);
    List<EmployeeTasksSchedule> findByEmployeeTasksEmployesAndEmployeeTasksScheduleEventJenisNotAndStatusInAndEmployeeTasksStatusAndDateTasksOrderByEmployeeTasksScheduleEventScheduleTimeStart(Employes employes,StatusRecord statusRecord4,  List<StatusRecord> statusRecords, StatusRecord statusRecord2, LocalDate localDate);


    List<EmployeeTasksSchedule> findByEmployeeTasksEmployesAndEmployeeTasksScheduleEventJenisNotAndStatusNotAndEmployeeTasksStatusAndDateTasksOrderByEmployeeTasksScheduleEventScheduleTimeStart(Employes employes,StatusRecord statusRecord4,  StatusRecord statusRecords, StatusRecord statusRecord2, LocalDate localDate);


    @Query(value = "select coalesce(sum(reward),0) as nominal,count(a.id) as total,sum(if(a.status = 'DONE', 1, 0)) as done from employee_tasks_schedule as a\n" +
            "inner join employee_tasks as b \n" +
            "on a.id_employee_tasks = b.id where b.id_employee = ?1 and a.date_tasks >= ?2 and a.date_tasks <= ?3", nativeQuery = true)
    Object jumlahNominalPegawai(String idEmployee, LocalDate tanggalMulai, LocalDate tanggalSelesai);




    @Query(value = "select persentase, if(persentase >= coalesce(grade_aa,80), 'A',if(persentase >= coalesce(grade_be,70),'B',\n" +
            "if(persentase >= coalesce(grade_ce,60),'C',if(persentase >= coalesce(grade_de,0),'D','E')))) as grade, total from\n" +
            "(select 1 as ada, count(id_employee) as total, sum(if(a.status = 'DONE',1,0)) as done,round((100*sum(if(a.status = 'DONE',1,0)))/count(id_employee),0) as persentase \n" +
            "from employee_tasks_schedule as a\n" +
            "inner join employee_tasks as b on a.id_employee_tasks = b.id\n" +
            "where a.date_tasks >= date(DATE_SUB(NOW(), INTERVAL 30 DAY)) and a.date_tasks <= date(now())\n" +
            "and b.id_employee = ?1 group by id_employee) as a\n" +
            "left join\n" +
            "(select 1 as ada, coalesce(grade_aa,80) as grade_aa, coalesce(grade_be,70)as grade_be,\n" +
            "coalesce(grade_ce,60)as grade_ce, coalesce(grade_de,0)as grade_de from tasks_score where \n" +
            "status = 'AKTIF' order by date_update desc limit 1) as b on a.ada = b.ada", nativeQuery = true)
    Object dataPrestasi(String idEmployee);


    @Query(value = "select c.schedule_name, count(id_employee_tasks) as total, sum(if(a.status = 'DONE',1,0)) as done,round((100*sum(if(a.status = 'DONE',1,0)))/count(id_employee),0) as persentase \n" +
            "from employee_tasks_schedule as a\n" +
            "inner join employee_tasks as b on a.id_employee_tasks = b.id\n" +
            "inner join schedule_event as c on b.id_schedule_event = c.id\n" +
            "where a.date_tasks >= ?2 and a.date_tasks <= ?3\n" +
            "and b.id_employee = ?1 group by b.id", nativeQuery = true)
    List<Object> dataProgress(String idEmployee, LocalDate tanggalMulai, LocalDate tanggalSelesai);


    @Query(value = "select if(a.status = 'DONE', '500' , '50') as clas, c.schedule_name as scheduleName, a.date_tasks as dateTasks, a.id \n" +
            "from employee_tasks_schedule as a\n" +
            "inner join employee_tasks as b on a.id_employee_tasks = b.id\n" +
            "inner join schedule_event as c on b.id_schedule_event = c.id\n" +
            "where a.date_tasks >= ?2 and a.date_tasks <= ?3\n" +
            "and b.id_employee = ?1 order by date_tasks, c.schedule_time_start", nativeQuery = true)
    List<TaskScheduleEmployeeReportsDto> dataAbsensiStatus(String idEmployee, LocalDate tanggalMulai, LocalDate tanggalSelesai);

    EmployeeTasksSchedule findByStatusAndEmployeeTasksIdAndDateTasks(StatusRecord statusRecord, String employeeTasks, LocalDate dateTasks);

    List<EmployeeTasksSchedule> findByStatusAndEmployeeTasksIdAndDateTasksOrderById(StatusRecord statusRecord, String employeeTasks, LocalDate dateTasks);


    @Query(value = "select a.id,full_name as fullName,schedule_name as scheduleName,coalesce(jumlah,0) as jumlah,c.reward as tarif, coalesce(sum(d.reward),0) as reward from employes as a\n" +
            "inner join employee_tasks as b \n" +
            "on a.id = b.id_employee\n" +
            "inner join schedule_event as c on b.id_schedule_event = c.id\n" +
            "left join(select id_employee_tasks, count(id) as jumlah, sum(reward) as reward \n" +
            "from employee_tasks_schedule where status = 'DONE' and date_tasks >= ?1 and date_tasks <= ?2 group by id_employee_tasks) d\n" +
            "on b.id = d.id_employee_tasks\n" +
            "where a.status = 'AKTIF' and b.status = 'AKTIF' group by b.id order by full_name, schedule_name", nativeQuery = true)
    List<PerformanceReportDto> performanceReport(LocalDate tanggalMulai, LocalDate tanggalSelesai);


    @Query(value = "select a.id,full_name as fullName, a.employee_class,coalesce(sum(d.reward),0) as totalreward from employes as a\n" +
            "inner join employee_tasks as b \n" +
            "on a.id = b.id_employee\n" +
            "inner join schedule_event as c on b.id_schedule_event = c.id\n" +
            "left join(select id_employee_tasks, count(id) as jumlah, sum(reward) as reward \n" +
            "from employee_tasks_schedule where status = 'DONE' and date_tasks >= ?1 and date_tasks <= ?2 group by id_employee_tasks) d\n" +
            "on b.id = d.id_employee_tasks\n" +
            "where a.status = 'AKTIF' and b.status = 'AKTIF' group by a.id order by full_name" , countQuery = "select count(id)as jml from\n" +
            "(select a.id,full_name as fullName,coalesce(sum(d.reward),0) as totalreward from employes as a\n" +
            "inner join employee_tasks as b \n" +
            "on a.id = b.id_employee\n" +
            "inner join schedule_event as c on b.id_schedule_event = c.id\n" +
            "left join(select id_employee_tasks, count(id) as jumlah, sum(reward) as reward \n" +
            "from employee_tasks_schedule where status = 'DONE' and date_tasks >= ?1 and date_tasks <= ?2 group by id_employee_tasks) d\n" +
            "on b.id = d.id_employee_tasks\n" +
            "where a.status = 'AKTIF' and b.status = 'AKTIF' group by a.id) as a", nativeQuery = true)
    Page<Object> daftarEmployeeAktif(LocalDate tanggalMulai, LocalDate tanggalSelesai, Pageable pageable);

}
