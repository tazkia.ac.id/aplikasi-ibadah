package id.ac.tazkia.ibadah.dao.config;

import id.ac.tazkia.ibadah.entity.config.User;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserDao extends PagingAndSortingRepository<User, String> {
    User findByUsername(String username);
}
