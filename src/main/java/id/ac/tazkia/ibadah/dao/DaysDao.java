package id.ac.tazkia.ibadah.dao;

import id.ac.tazkia.ibadah.entity.Days;
import id.ac.tazkia.ibadah.entity.StatusRecord;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface DaysDao extends CrudRepository<Days, String>, PagingAndSortingRepository<Days, String> {

    Days findByStatusAndNumberDay(StatusRecord statusRecord, Integer namaHari);

}
