package id.ac.tazkia.ibadah.dao.schedule;

import id.ac.tazkia.ibadah.dto.CariTasksDto;
import id.ac.tazkia.ibadah.dto.GenerateTanggalDto;
import id.ac.tazkia.ibadah.dto.ScheduleEventEmployeeDto;
import id.ac.tazkia.ibadah.entity.EmployeeClass;
import id.ac.tazkia.ibadah.entity.ScheduleType;
import id.ac.tazkia.ibadah.entity.StatusRecord;
import id.ac.tazkia.ibadah.entity.schedule.ScheduleEvent;
import org.springframework.cglib.core.Local;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.lang.reflect.Field;
import java.text.Format;
import java.time.LocalDate;
import java.util.List;

public interface ScheduleEventDao extends CrudRepository<ScheduleEvent,String>,PagingAndSortingRepository<ScheduleEvent, String> {

    ScheduleEvent findByStatusAndId(StatusRecord statusRecord, String id);
    Page<ScheduleEvent> findByStatusAndJenisNotOrderByScheduleName(StatusRecord statusRecord,StatusRecord statusRecord2, Pageable pageable);

    Page<ScheduleEvent> findByStatusAndJenisOrderByScheduleNameAsc(StatusRecord statusRecord, StatusRecord jenisEvent, Pageable pageable);


    Page<ScheduleEvent> findByStatusAndJenisOrderByScheduleDateDesc(StatusRecord statusRecord, StatusRecord jenisEvent, Pageable pageable);

    List<ScheduleEvent> findByStatusAndJenisNotOrderByScheduleName(StatusRecord statusRecord, StatusRecord statusRecord2);

    List<ScheduleEvent> findByStatusAndJenisNotAndTaskContainingIgnoreCaseOrderByScheduleName(StatusRecord statusRecord, StatusRecord statusRecord2, String task);

    Page<ScheduleEvent> findByStatusAndJenisNotAndTaskContainingIgnoreCaseOrStatusAndJenisNotAndScheduleNameContainingIgnoreCaseOrderByScheduleName(StatusRecord statusRecord, StatusRecord statusRecord1, String cari1, StatusRecord statusRecord2, StatusRecord statusRecord4, String cari,  Pageable pageable);

    Page<ScheduleEvent> findByJenisNotAndTaskContainingIgnoreCaseOrStatusAndJenisNotAndScheduleNameContainingIgnoreCaseOrderByScheduleName(StatusRecord statusRecord1, String cari1, StatusRecord statusRecord2, StatusRecord statusRecord4, String cari,  Pageable pageable);

    Page<ScheduleEvent> findByJenisNotOrderByScheduleName(StatusRecord statusRecord2, Pageable pageable);


    @Query(value = "select a.*, b.name_day as nameDay,field_name as fieldName from\n" +
            "(SELECT gen_date as genDate, DAYOFWEEK(gen_date) AS hari FROM\n" +
            "(SELECT ADDDATE(?1,t4*10000 + t3*1000 + t2*100 + t1*10 + t0) gen_date FROM\n" +
            "(SELECT 0 t0 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t0,\n" +
            "(SELECT 0 t1 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t1,\n" +
            "(SELECT 0 t2 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t2,\n" +
            "(SELECT 0 t3 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t3,\n" +
            "(SELECT 0 t4 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t4) v\n" +
            "WHERE gen_date BETWEEN ?1 AND ?2) as a\n" +
            "left join days as b on a.hari = b.number_day+1 order by genDate;", nativeQuery = true)
    List<GenerateTanggalDto> generateTanggal(LocalDate tanggalMulai, LocalDate tanggalSelesai);


//    @Query(value = "select a.id, id_employee as idEmployee,id_schedule_event as idScheduleEvent, reward from employee_tasks as a \n" +
//            "inner join schedule_event as b on a.id_schedule_event = b.id \n" +
//            "where a.status = 'AKTIF' and b.status = 'AKTIF' and ?1 = 'AKTIF' and b.schedule_type in ('Custom','Daily');", nativeQuery = true)
//    List<CariTasksDto> cariTaskEmployee(String hari);
//
//

    List<ScheduleEvent> findByStatusAndStatusAktifAndJenisAndScheduleDate(StatusRecord statusRecord, StatusRecord statusRecord2,StatusRecord statusRecord3, LocalDate localDate);

    List<ScheduleEvent> findByStatusAndScheduleDateAndStatusAktifAndScheduleType(StatusRecord statusRecord, LocalDate localDate, StatusRecord statusRecord2, ScheduleType scheduleType);

    @Query(value = "select a.id, a.schedule_date as scheduleDate, a.schedule_time_start as scheduleTimeStart, schedule_time_end as scheduleTimeEnd,\n" +
            "schedule_name as scheduleName, schedule_description as scheduleDescription, schedule_type as scheduleType, coalesce(upload_picture,'NONAKTIF') as uploadPicture,\n" +
            "coalesce(must_match,'NONAKTIF') as mustMatch, a.reward, jenis ,coalesce(c.status,'WAITING')as statusAbsen,c.description as absenDescription from\n" +
            "(select * from schedule_event as a\n" +
            "where a.status = 'AKTIF' and a.status_aktif = 'AKTIF' and a.jenis = 'EVENT' and employee_class = ?3 and schedule_date = ?1)as a\n" +
            "left join\n" +
            "(select id, id_schedule_event from employee_tasks where status = 'AKTIF' and id_employee = ?2) as b on a.id = b.id_schedule_event\n" +
            "left join\n" +
            "(select id, id_employee_tasks, status,description from employee_tasks_schedule where status <> 'HAPUS') as c on b.id = c.id_employee_tasks",nativeQuery = true)
    List<ScheduleEventEmployeeDto> cariEventEmployee(LocalDate tanggal, String idEmployee, String employeeClass);

}
