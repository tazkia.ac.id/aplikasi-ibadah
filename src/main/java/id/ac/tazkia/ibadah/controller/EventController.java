package id.ac.tazkia.ibadah.controller;

import id.ac.tazkia.ibadah.dao.config.UserDao;
import id.ac.tazkia.ibadah.dao.masterdata.EmployesDao;
import id.ac.tazkia.ibadah.dao.schedule.ScheduleEventDao;
import id.ac.tazkia.ibadah.entity.EmployeeClass;
import id.ac.tazkia.ibadah.entity.ScheduleType;
import id.ac.tazkia.ibadah.entity.StatusRecord;
import id.ac.tazkia.ibadah.entity.config.User;
import id.ac.tazkia.ibadah.entity.masterdata.Employes;
import id.ac.tazkia.ibadah.entity.schedule.ScheduleEvent;
import id.ac.tazkia.ibadah.services.CurrentUserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.core.Local;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

@Controller
public class EventController {

    @Autowired
    private ScheduleEventDao scheduleEventDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    CurrentUserService currentUserService;

    @GetMapping("/event")
    public String eventPage(Model model,
                            Authentication authentication,
                            @RequestParam(required = false)String search, @PageableDefault Pageable pageable){

        User user = currentUserService.currentUser(authentication);
        Employes userEmployes = employesDao.findByUser(user);
        model.addAttribute("userEmployes", userEmployes);

        model.addAttribute("list", scheduleEventDao.findByStatusAndJenisOrderByScheduleDateDesc(StatusRecord.AKTIF, StatusRecord.EVENT, pageable));
        return "events/eventList";
    }

    @GetMapping("/event/add")
    public String eventAddPage(Model model, Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes userEmployes = employesDao.findByUser(user);
        model.addAttribute("userEmployes", userEmployes);

        model.addAttribute("scheduleEvent", new ScheduleEvent());
        model.addAttribute("listEmployeeClass", EmployeeClass.values());

        return "events/eventAdd";
    }

    @PostMapping("/event/save")
    public String eventSave(@Valid ScheduleEvent scheduleEvent, Authentication authentication, RedirectAttributes attributes){

        User user = currentUserService.currentUser(authentication);
        Employes userEmployes = employesDao.findByUser(user);

        String tanggal = scheduleEvent.getScheduleDateString();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        LocalDate localDate = LocalDate.parse(tanggal, formatter);
        LocalTime timeStart = LocalTime.parse(scheduleEvent.getScheduleTimeStartString());
        LocalTime timeEnd = LocalTime.parse(scheduleEvent.getScheduleTimeEndString());

        scheduleEvent.setStatus(StatusRecord.AKTIF);
        scheduleEvent.setScheduleDate(localDate);
        scheduleEvent.setScheduleTimeStart(timeStart);
        scheduleEvent.setScheduleTimeEnd(timeEnd);
        scheduleEvent.setStatusAktif(StatusRecord.AKTIF);
        scheduleEvent.setScheduleType(ScheduleType.Once);
        scheduleEvent.setJenis(StatusRecord.EVENT);
        scheduleEvent.setUserInsert(userEmployes.getId());
        scheduleEvent.setDateInsert(LocalDateTime.now());
        scheduleEventDao.save(scheduleEvent);

        attributes.addFlashAttribute("success", "Event Successfully Added");
        return "redirect:/event";
    }

    @GetMapping("/event/edit")
    public String eventEditPage(Model model,
            Authentication authentication,@RequestParam(name = "Id") ScheduleEvent idEvent){
        User user = currentUserService.currentUser(authentication);
        Employes userEmployes = employesDao.findByUser(user);
        model.addAttribute("userEmployes", userEmployes);
        model.addAttribute("scheduleEvent", idEvent);
        model.addAttribute("listEmployeeClass", EmployeeClass.values());
        return "events/eventEdit";
    }

    @PostMapping("/event/update")
    public String eventUpdate(@ModelAttribute ScheduleEvent scheduleEvent,
                              Authentication authentication,
                              RedirectAttributes redirectAttributes){

        User user = currentUserService.currentUser(authentication);
        Employes userEmployes = employesDao.findByUser(user);

        LocalDate localDate = LocalDate.now();
        if (scheduleEvent.getScheduleDateString().length() > 0) {
            String tanggal = scheduleEvent.getScheduleDateString();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
            localDate = LocalDate.parse(tanggal, formatter);
        }
        LocalTime timeStart = LocalTime.parse(scheduleEvent.getScheduleTimeStartString());
        LocalTime timeEnd = LocalTime.parse(scheduleEvent.getScheduleTimeEndString());

        if(scheduleEvent.getStatus() == null){
            scheduleEvent.setStatusAktif(StatusRecord.NONAKTIF);
        }

        scheduleEvent.setScheduleTimeStart(timeStart);
        scheduleEvent.setScheduleTimeEnd(timeEnd);
        scheduleEvent.setScheduleDate(localDate);
        scheduleEvent.setScheduleType(ScheduleType.Once);
        scheduleEvent.setDateUpdate(LocalDateTime.now());
        scheduleEvent.setUserUpdate(userEmployes.getId());
        scheduleEventDao.save(scheduleEvent);

        redirectAttributes.addFlashAttribute("success", "Event Successfully Update");
        return "redirect:/event";
    }

}
