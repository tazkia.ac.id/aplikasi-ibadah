package id.ac.tazkia.ibadah.controller;

import id.ac.tazkia.ibadah.dao.masterdata.EmployesDao;
import id.ac.tazkia.ibadah.dao.setting.TasksScoreDao;
import id.ac.tazkia.ibadah.entity.StatusRecord;
import id.ac.tazkia.ibadah.entity.config.User;
import id.ac.tazkia.ibadah.entity.masterdata.Employes;
import id.ac.tazkia.ibadah.entity.setting.TasksScore;
import id.ac.tazkia.ibadah.services.CurrentUserService;
import org.mortbay.thread.Timeout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDateTime;

@Controller
public class ScoreController {

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private TasksScoreDao tasksScoreDao;

    @GetMapping("/score-setting")
    public String scoreSettingPage(Model model,
                                   Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Employes userEmployes = employesDao.findByUser(user);
        model.addAttribute("userEmployes", userEmployes);

        TasksScore tasksScore = tasksScoreDao.findTopByStatusOrderByDateUpdateDesc(StatusRecord.AKTIF);
        if(tasksScore != null){
            model.addAttribute("taskScore", tasksScore);
        }else{
            model.addAttribute("taskScore", new TasksScore());
        }

        return "scoreSetting";
    }

    @PostMapping("/score-setting/save")
    public String scoreSettingSave(@ModelAttribute TasksScore tasksScore,
                                   Authentication authentication,
                                   RedirectAttributes redirectAttributes){

        User user = currentUserService.currentUser(authentication);
        Employes userEmployes = employesDao.findByUser(user);

        tasksScore.setDateUpdate(LocalDateTime.now());
        tasksScore.setStatus(StatusRecord.AKTIF);
        tasksScore.setUserUpdate(userEmployes.getFullName());
        tasksScoreDao.save(tasksScore);

        redirectAttributes.addFlashAttribute("sukses", "Data Berhasil diupdate");
        return "redirect:../score-setting";
    }
}
