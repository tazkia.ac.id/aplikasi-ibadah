package id.ac.tazkia.ibadah.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class AuthController {
//    @GetMapping("/signin")
//    public String loginPage() {
//        return "signin";
//    }

    @RequestMapping(value = "/signin", method = RequestMethod.GET)
    public String login(Model model) {
        model.addAttribute("loginError", true);
        return "signin";
    }



}


