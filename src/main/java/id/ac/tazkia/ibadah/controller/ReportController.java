package id.ac.tazkia.ibadah.controller;

import id.ac.tazkia.ibadah.dao.config.UserDao;
import id.ac.tazkia.ibadah.dao.masterdata.EmployesDao;
import id.ac.tazkia.ibadah.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.ibadah.dao.setting.EmployeeTaskScheduleDao;
import id.ac.tazkia.ibadah.dao.setting.PayrollPeriodeDao;
import id.ac.tazkia.ibadah.entity.StatusRecord;
import id.ac.tazkia.ibadah.entity.config.User;
import id.ac.tazkia.ibadah.entity.masterdata.Employes;
import id.ac.tazkia.ibadah.entity.setting.PayrollPeriode;
import id.ac.tazkia.ibadah.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@Controller
public class ReportController {

    @Autowired
    private UserDao userDao;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    CurrentUserService currentUserService;

    @Autowired
    private PayrollPeriodeDao payrollPeriodeDao;

    @Autowired
    private EmployeeTaskScheduleDao employeeTaskScheduleDao;

    @Autowired
    private EmployeeJobPositionDao employeeJobPositionDao;


    @GetMapping("/reports")
    public String reportDetailPage(Model model,
                                   Authentication authentication,
                                   @RequestParam(required = false)String start,
                                   @RequestParam(required = false)String end,
                                   @RequestParam(required = false)String search,
                                   @PageableDefault Pageable pageable){

        User user = currentUserService.currentUser(authentication);
        Employes userEmployes = employesDao.findByUser(user);
        model.addAttribute("userEmployes", userEmployes);

        PayrollPeriode payrollPeriode = payrollPeriodeDao.findByStatus(StatusRecord.AKTIF);

        LocalDate tanggal = LocalDate.now();
        String tanggalMulaiHitung=tanggal.toString();
        String tanggalSelesaiHitung=tanggal.toString();
        String tanggalMulaiLayout = tanggal.toString();
        String tanggalSelesaiLayout = tanggal.toString();

        if(start != null && end != null){
            tanggalMulaiHitung = start;
            tanggalSelesaiHitung = end;
            tanggalMulaiLayout = start;
            tanggalSelesaiLayout = end;
        }else {
            String bulanMulai = tanggal.getMonthValue() + "";
            if (bulanMulai.length() == 1) {
                bulanMulai = "0" + bulanMulai;
            }
            String bulanSelesai = tanggal.plusMonths(1).getMonthValue() + "";
            if (bulanSelesai.length() == 1) {
                bulanSelesai = "0" + bulanSelesai;
            }
            String tahunMulai = tanggal.getYear() + "";
            String tahunSelesai = tanggal.plusMonths(1).getYear() + "";

            String dayMulai = LocalDate.now().getDayOfMonth() + "";
            if (dayMulai.length() == 1) {
                dayMulai = "0" + dayMulai;
            }


            if (tanggal.getDayOfMonth() <= 20) {
                String month1 = tanggal.minusMonths(1).getMonthValue() + "";
                String month2 = tanggal.getMonthValue() + "";
                if (month1.length() == 1) {
                    month1 = "0" + month1;
                }
                if (month2.length() == 1) {
                    month2 = "0" + month2;
                }
                if (tanggal.getMonthValue() == 1) {
                    tanggalMulaiHitung = tanggal.minusYears(1).getYear() + "-" + month1 + "-" + payrollPeriode.getDariTanggal();
                    tanggalMulaiLayout = month1 + "/" + payrollPeriode.getDariTanggal() + "/" + tanggal.minusYears(1).getYear();
                    tanggalSelesaiHitung = tanggal.getYear() + "-" + month2 + "-" + payrollPeriode.getSampaiTanggal();
                    tanggalSelesaiLayout = month2 + "/" + payrollPeriode.getSampaiTanggal() + "/" + tanggal.getYear();
                } else {

                    tanggalMulaiHitung = tanggal.getYear() + "-" + month1 + "-" + payrollPeriode.getDariTanggal();
                    tanggalMulaiLayout = month1 + "/" + payrollPeriode.getDariTanggal() + "/" + tanggal.getYear();
                    tanggalSelesaiHitung = tanggal.getYear() + "-" + month2 + "-" + payrollPeriode.getSampaiTanggal();
                    tanggalSelesaiLayout = month2 + "/" + payrollPeriode.getSampaiTanggal() + "/" + tanggal.getYear();
                }
            } else {
                String month1 = tanggal.getMonthValue() + "";
                String month2 = tanggal.plusMonths(1).getMonthValue() + "";
                if (month1.length() == 1) {
                    month1 = "0" + month1;
                }
                if (month2.length() == 1) {
                    month2 = "0" + month2;
                }
                if (tanggal.getMonthValue() == 12) {
                    tanggalMulaiHitung = tanggal.getYear() + "-" + month1 + "-" + payrollPeriode.getDariTanggal();
                    tanggalMulaiLayout = month1 + "/" + payrollPeriode.getDariTanggal() + "/" + tanggal.getYear();
                    tanggalSelesaiHitung = tanggal.plusYears(1).getYear() + "-01-" + payrollPeriode.getSampaiTanggal();
                    tanggalSelesaiLayout = "01/" + payrollPeriode.getSampaiTanggal() + "/" + tanggal.plusYears(1).getYear();
                } else {
                    tanggalMulaiHitung = tanggal.getYear() + "-" + month1 + "-" + payrollPeriode.getDariTanggal();
                    tanggalMulaiLayout = month1 + "/" + payrollPeriode.getDariTanggal() + "/" + tanggal.getYear();
                    tanggalSelesaiHitung = tanggal.getYear() + "-" + month2 + "-" + payrollPeriode.getSampaiTanggal();
                    tanggalSelesaiLayout = month2 + "/" + payrollPeriode.getSampaiTanggal() + "/" + tanggal.getYear();
                }
            }
        }

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        LocalDate localDateMulaiHitung = LocalDate.parse(tanggalMulaiLayout, formatter2);
        LocalDate localDateSelesaiHitung = LocalDate.parse(tanggalSelesaiLayout, formatter2);
        if(start != null && end != null){
             localDateMulaiHitung = LocalDate.parse(tanggalMulaiHitung, formatter2);
             localDateSelesaiHitung = LocalDate.parse(tanggalSelesaiHitung, formatter2);
        }else{
             localDateMulaiHitung = LocalDate.parse(tanggalMulaiHitung, formatter);
             localDateSelesaiHitung = LocalDate.parse(tanggalSelesaiHitung, formatter);
        }

        LocalDate localDateMulaiLayout = LocalDate.parse(tanggalMulaiLayout, formatter2);
        LocalDate localDateSelesaiLayout = LocalDate.parse(tanggalSelesaiLayout, formatter2);


        Object totalTasksNominal = employeeTaskScheduleDao.jumlahNominalPegawai(userEmployes.getId(), localDateMulaiHitung, localDateSelesaiHitung);
        model.addAttribute("totalNominal", totalTasksNominal);
        model.addAttribute("tanggalMulai", localDateMulaiHitung);
        model.addAttribute("tanggalSelesai", localDateSelesaiHitung);
        model.addAttribute("dataPrestasi", employeeTaskScheduleDao.dataPrestasi(userEmployes.getId()));
        model.addAttribute("dataProgress", employeeTaskScheduleDao.dataProgress(userEmployes.getId(), localDateMulaiHitung, localDateSelesaiHitung));
        model.addAttribute("tanggalMulai", localDateMulaiLayout);
        model.addAttribute("tanggalSelesai", localDateSelesaiLayout);
        model.addAttribute("listAbsenStatus", employeeTaskScheduleDao.dataAbsensiStatus(userEmployes.getId(), localDateMulaiHitung, localDateSelesaiHitung));
        System.out.println(localDateMulaiLayout);
        System.out.println(localDateSelesaiLayout);

        return "reports";
    }

    @GetMapping("/performance-report")
    public String performanceReportPage(Model model,
                                        @RequestParam(required = false) String search,
                                        @RequestParam(required = false) String localDateMulai1,
                                        @RequestParam(required = false) String localDateSelesai1,
                                        Authentication authentication,
                                        @PageableDefault(size = 10) Pageable page) {
        User user = currentUserService.currentUser(authentication);
        Employes userEmployes = employesDao.findByUser(user);

        PayrollPeriode payrollPeriode = payrollPeriodeDao.findByStatus(StatusRecord.AKTIF);


        LocalDate tanggal = LocalDate.now();
        String bulanMulai = tanggal.minusMonths(1).getMonthValue()+"";
        if(tanggal.getMonthValue() == 1){
            bulanMulai = "12";
        }
        if(bulanMulai.length() == 1 ){
            bulanMulai = "0" + bulanMulai;
        }
        String bulanSelesai = tanggal.getMonthValue()+"";
        if(bulanSelesai.length() == 1 ){
            bulanSelesai = "0" + bulanSelesai;
        }
        String tahunMulai = tanggal.minusMonths(1).getYear()+"";
        if(tanggal.getMonthValue() == 1){
            tahunMulai = tanggal.minusYears(1).getYear()+"";
        }
        String tahunSelesai = tanggal.getYear()+"";

        String dayMulai = LocalDate.now().getDayOfMonth()+"";
        if(dayMulai.length() == 1 ){
            dayMulai = "0" + dayMulai;
        }

        String tanggalMulai = tahunMulai+'-'+bulanMulai+'-'+ payrollPeriode.getDariTanggal();
        String tanggalSelesai = tahunSelesai+'-'+bulanSelesai+'-'+payrollPeriode.getSampaiTanggal();

        String tanggalMulai2 = bulanMulai+'/'+ payrollPeriode.getDariTanggal()+'/'+tahunMulai;
        String tanggalSelesai2 = bulanSelesai+'/'+payrollPeriode.getSampaiTanggal()+'/'+tahunSelesai;
        if (StringUtils.hasText(localDateMulai1)) {
            tanggalMulai2 = localDateMulai1;
        }
        if(StringUtils.hasText(localDateSelesai1)){
            tanggalSelesai2 = localDateSelesai1;
        }

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("MM/dd/yyyy");

        LocalDate localDateMulai = LocalDate.parse(tanggalMulai, formatter);
        LocalDate localDateSelesai = LocalDate.parse(tanggalSelesai, formatter);
        if(StringUtils.hasText(localDateMulai1) && StringUtils.hasText(localDateSelesai1)){
            localDateMulai = LocalDate.parse(localDateMulai1, formatter2);
            localDateSelesai = LocalDate.parse(localDateSelesai1, formatter2);
        }
        LocalDate localDateMulai2 = LocalDate.parse(tanggalMulai2, formatter2);
        LocalDate localDateSelesai2 = LocalDate.parse(tanggalSelesai2, formatter2);

        System.out.println("Tanggal Mulai : "+ localDateMulai2);
        System.out.println("Tanggal Selesai : " + localDateSelesai2);
        model.addAttribute("localDateMulai", tanggalMulai2);
        model.addAttribute("localDateSelesai", tanggalSelesai2);
//        model.addAttribute("listEmployee", employeeJobPositionDao.cariNamaKrywanaScheduleTasks(page));


        model.addAttribute("listEmployee", employeeTaskScheduleDao.daftarEmployeeAktif(localDateMulai, localDateSelesai,page));
        model.addAttribute("listPerformanceReport", employeeTaskScheduleDao.performanceReport(localDateMulai, localDateSelesai));

        model.addAttribute("userEmployes", userEmployes);
        return "performanceReport";
    }

    @GetMapping("overview")
    public String overviewPage(Model model,Authentication authentication) {
        User user = currentUserService.currentUser(authentication);
        Employes userEmployes = employesDao.findByUser(user);
        model.addAttribute("userEmployes", userEmployes);

        return "overview";
    }
}