package id.ac.tazkia.ibadah.controller;

import id.ac.tazkia.ibadah.dao.config.UserDao;
import id.ac.tazkia.ibadah.dao.masterdata.EmployesDao;
import id.ac.tazkia.ibadah.dao.schedule.ScheduleEventDao;
import id.ac.tazkia.ibadah.dao.setting.EmployeeTaskScheduleDao;
import id.ac.tazkia.ibadah.dao.setting.EmployeeTasksDao;
import id.ac.tazkia.ibadah.entity.StatusRecord;
import id.ac.tazkia.ibadah.entity.config.User;
import id.ac.tazkia.ibadah.entity.masterdata.Employes;
import id.ac.tazkia.ibadah.entity.schedule.ScheduleEvent;
import id.ac.tazkia.ibadah.entity.setting.EmployeeTasks;
import id.ac.tazkia.ibadah.entity.setting.EmployeeTasksSchedule;
import id.ac.tazkia.ibadah.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Conditional;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@Controller
public class PresensiTasksController {

    @Autowired
    private EmployeeTaskScheduleDao employeeTaskScheduleDao;
    @Autowired
    private ScheduleEventDao scheduleEventDao;
    @Autowired
    private CurrentUserService currentUserService;
    @Autowired
    private UserDao userDao;
    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private EmployeeTasksDao employeeTasksDao;

    @PostMapping("/savepresensi")
    public String savePresensi(@RequestParam(required = true) EmployeeTasksSchedule employeeTasksSchedule,
                               @RequestParam(required = false) String description,
                               @RequestParam(required = false) StatusRecord status,
                               @RequestParam(value = "fileUpload", required = false) MultipartFile file,
                               Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);

        System.out.println("Tasks : "+ employeeTasksSchedule);
        System.out.println("Description : "+ description);
        System.out.println("Status : "+ status);
        if(status == null){
            employeeTasksSchedule.setStatus(StatusRecord.WAITING);
            employeeTasksSchedule.setReward(BigDecimal.ZERO);
        }else{
            employeeTasksSchedule.setStatus(StatusRecord.DONE);
            employeeTasksSchedule.setReward(employeeTasksSchedule.getEmployeeTasks().getScheduleEvent().getReward());
        }
        employeeTasksSchedule.setDescription(description);
        employeeTasksSchedule.setDateLog(LocalDateTime.now());
        employeeTasksSchedule.setEmployes(employes);
        employeeTaskScheduleDao.save(employeeTasksSchedule);

        return "redirect:/home";
    }

    @PostMapping("/savepresensievent")
    public String savePresensiEvent(@RequestParam(required = true)ScheduleEvent scheduleEvent,
                                    @RequestParam(value = "fileUpload", required = false) MultipartFile file,
                                    @RequestParam(required = false) String status,
                                    @RequestParam(required = false) String description,
                                    Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Employes employes = employesDao.findByUser(user);
        Employes userEmployes = employesDao.findByUser(user);

        EmployeeTasks employeeTasks = employeeTasksDao.findByStatusAndEmployesAndScheduleEvent(StatusRecord.AKTIF, employes, scheduleEvent);
        if(employeeTasks == null){
            EmployeeTasks employeeTasks1 = new EmployeeTasks();
            String idEmployeeTasks = UUID.randomUUID().toString();
            employeeTasks1.setId(idEmployeeTasks);
            employeeTasks1.setScheduleEvent(scheduleEvent);
            employeeTasks1.setEmployes(employes);
            employeeTasks1.setStatus(StatusRecord.AKTIF);
            employeeTasks1.setUserInsert(employes.getUser().getUsername());
            employeeTasks1.setTanggalInsert(LocalDateTime.now());
            employeeTasksDao.save(employeeTasks1);
            System.out.println("Task : "+ employeeTasks1);
            EmployeeTasksSchedule employeeTasksSchedule = employeeTaskScheduleDao.findByEmployeeTasksAndStatusNot(employeeTasks1, StatusRecord.HAPUS);
                if(employeeTasksSchedule == null){
                    EmployeeTasksSchedule employeeTasksSchedule1 = new EmployeeTasksSchedule();
                    employeeTasksSchedule1.setEmployeeTasks(employeeTasks1);
                    employeeTasksSchedule1.setDescription(description);
                    employeeTasksSchedule1.setDateTasks(LocalDate.now());
                    employeeTasksSchedule1.setStatus(StatusRecord.DONE);
                    employeeTasksSchedule1.setReward(employeeTasks1.getScheduleEvent().getReward());
                    employeeTasksSchedule1.setDateLog(LocalDateTime.now());
                    employeeTasksSchedule1.setEmployes(employes);
                    employeeTaskScheduleDao.save(employeeTasksSchedule1);
                }else{
                    if(status == null){
                        employeeTasksSchedule.setDescription(description);
                        employeeTasksSchedule.setDateTasks(LocalDate.now());
                        employeeTasksSchedule.setStatus(StatusRecord.WAITING);
                        employeeTasksSchedule.setReward(BigDecimal.ZERO);
                        employeeTasksSchedule.setDateLog(LocalDateTime.now());
                        employeeTasksSchedule.setEmployes(employes);
                        employeeTaskScheduleDao.save(employeeTasksSchedule);
                    }else{
                        employeeTasksSchedule.setDescription(description);
                        employeeTasksSchedule.setDateTasks(LocalDate.now());
                        employeeTasksSchedule.setStatus(StatusRecord.DONE);
                        employeeTasksSchedule.setReward(employeeTasks1.getScheduleEvent().getReward());
                        employeeTasksSchedule.setDateLog(LocalDateTime.now());
                        employeeTasksSchedule.setEmployes(employes);
                        employeeTaskScheduleDao.save(employeeTasksSchedule);
                    }
                }
        }else{
            EmployeeTasksSchedule employeeTasksSchedule = employeeTaskScheduleDao.findByEmployeeTasksAndStatusNot(employeeTasks, StatusRecord.HAPUS);
            if(employeeTasksSchedule == null){
                EmployeeTasksSchedule employeeTasksSchedule1 = new EmployeeTasksSchedule();
                employeeTasksSchedule1.setEmployeeTasks(employeeTasks);
                employeeTasksSchedule1.setDescription(description);
                employeeTasksSchedule1.setDateTasks(LocalDate.now());
                employeeTasksSchedule1.setStatus(StatusRecord.DONE);
                employeeTasksSchedule1.setReward(employeeTasks.getScheduleEvent().getReward());
                employeeTasksSchedule1.setDateLog(LocalDateTime.now());
                employeeTasksSchedule1.setEmployes(employes);
                employeeTaskScheduleDao.save(employeeTasksSchedule1);
            }else{
                if(status == null){
                    employeeTasksSchedule.setDescription(description);
                    employeeTasksSchedule.setDateTasks(LocalDate.now());
                    employeeTasksSchedule.setStatus(StatusRecord.WAITING);
                    employeeTasksSchedule.setReward(BigDecimal.ZERO);
                    employeeTasksSchedule.setDateLog(LocalDateTime.now());
                    employeeTasksSchedule.setEmployes(employes);
                    employeeTaskScheduleDao.save(employeeTasksSchedule);
                }else{
                    employeeTasksSchedule.setDescription(description);
                    employeeTasksSchedule.setDateTasks(LocalDate.now());
                    employeeTasksSchedule.setStatus(StatusRecord.DONE);
                    employeeTasksSchedule.setReward(employeeTasks.getScheduleEvent().getReward());
                    employeeTasksSchedule.setDateLog(LocalDateTime.now());
                    employeeTasksSchedule.setEmployes(employes);
                    employeeTaskScheduleDao.save(employeeTasksSchedule);
                }
            }
        }
        
        return "redirect:/home";
    }

}
