package id.ac.tazkia.ibadah.controller;

import id.ac.tazkia.ibadah.dao.masterdata.CompaniesDao;
import id.ac.tazkia.ibadah.dao.masterdata.EmployesDao;
import id.ac.tazkia.ibadah.dao.schedule.ScheduleEventDao;
import id.ac.tazkia.ibadah.dao.setting.EmployeeJobPositionDao;
import id.ac.tazkia.ibadah.entity.ScheduleType;
import id.ac.tazkia.ibadah.entity.StatusRecord;
import id.ac.tazkia.ibadah.entity.config.User;
import id.ac.tazkia.ibadah.entity.masterdata.Companies;
import id.ac.tazkia.ibadah.entity.masterdata.Employes;
import id.ac.tazkia.ibadah.entity.schedule.ScheduleEvent;
import id.ac.tazkia.ibadah.services.CurrentUserService;
import jakarta.validation.Valid;
import org.hibernate.Length;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

@Controller
public class TemplateTaskController {

    @Autowired
    private ScheduleEventDao scheduleEventDao;

    @Autowired
    private CompaniesDao companiesDao;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    private EmployeeJobPositionDao employeeJobPositionDao;

    @Autowired
    private CurrentUserService currentUserService;

    @GetMapping("task/templates")
    public String taskTemplatesPage(Model model,
                                    @RequestParam(required = false) String status,
                                    @PageableDefault(size = 10) Pageable page,
                                    @RequestParam(required = false)String search,
                                    Authentication authentication) {
        User user = currentUserService.currentUser(authentication);
        Employes userEmployes = employesDao.findByUser(user);
        model.addAttribute("userEmployes", userEmployes);

        if (StringUtils.hasText(status)) {
            model.addAttribute("status", status);
            if(status.equals("AKTIF")){
                if (StringUtils.hasText(search)) {
                    model.addAttribute("search", search);
                    model.addAttribute("listTask", scheduleEventDao.findByStatusAndJenisNotAndTaskContainingIgnoreCaseOrStatusAndJenisNotAndScheduleNameContainingIgnoreCaseOrderByScheduleName(StatusRecord.AKTIF, StatusRecord.EVENT, search, StatusRecord.AKTIF, StatusRecord.EVENT, search, page));
                } else {
                    model.addAttribute("listTask", scheduleEventDao.findByStatusAndJenisNotOrderByScheduleName(StatusRecord.AKTIF, StatusRecord.EVENT, page));
                }
            }else{
                if (StringUtils.hasText(search)) {
                    model.addAttribute("search", search);
                    model.addAttribute("listTask", scheduleEventDao.findByStatusAndJenisNotAndTaskContainingIgnoreCaseOrStatusAndJenisNotAndScheduleNameContainingIgnoreCaseOrderByScheduleName(StatusRecord.NONAKTIF, StatusRecord.EVENT, search, StatusRecord.NONAKTIF, StatusRecord.EVENT, search, page));
                } else {
                    model.addAttribute("listTask", scheduleEventDao.findByStatusAndJenisNotOrderByScheduleName(StatusRecord.NONAKTIF, StatusRecord.EVENT, page));
                }
            }
        }else {
            if (StringUtils.hasText(search)) {
                model.addAttribute("search", search);
                model.addAttribute("listTask", scheduleEventDao.findByJenisNotAndTaskContainingIgnoreCaseOrStatusAndJenisNotAndScheduleNameContainingIgnoreCaseOrderByScheduleName(StatusRecord.EVENT, search, StatusRecord.AKTIF, StatusRecord.EVENT, search, page));
            } else {
                model.addAttribute("listTask", scheduleEventDao.findByJenisNotOrderByScheduleName(StatusRecord.EVENT, page));
            }
        }

        return "templateTasks/templateTask";
    }

    @GetMapping("/task/template/add")
    public String addTaskTemplatePage(Model model,
                                      Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Employes userEmployes = employesDao.findByUser(user);
        model.addAttribute("userEmployes", userEmployes);
        model.addAttribute("scheduleTypeList", ScheduleType.values());
        model.addAttribute("scheduleEvent", new ScheduleEvent());

        return "templateTasks/templateAdd";
    }

    @PostMapping("/task/template/save")
    public String saveTaskTemplate(@ModelAttribute ScheduleEvent scheduleEvent,
                                   Authentication authentication,
                                   RedirectAttributes redirectAttributes){

        User user = currentUserService.currentUser(authentication);
        Employes userEmployes = employesDao.findByUser(user);

        Companies companies = companiesDao.findByStatusAndId(StatusRecord.AKTIF,"b5c2e2a-b711-4037-8f60-1e1aa0415958");

        System.out.println("Monday : " + scheduleEvent.getMonday());
        System.out.println("Sunday : " + scheduleEvent.getSunday());

        String tanggal = scheduleEvent.getScheduleDateString();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        if(tanggal.equals("")){
            String bulan = "01";
            String tgl = "01";
            if(LocalDate.now().getMonthValue() < 10){
                bulan = "0"+LocalDate.now().getMonthValue();
            }else{
                bulan = ""+ LocalDate.now().getMonthValue();
            }
            if(LocalDate.now().getDayOfMonth() < 10){
                tgl = "0"+LocalDate.now().getDayOfMonth();
            }else{
                tgl = ""+ LocalDate.now().getDayOfMonth();
            }
            tanggal = bulan +"/"+tgl+"/"+LocalDate.now().getYear();
        }

//        if(tanggal == null){
//            tanggal = LocalDate.now().getMonth().toString()+'/'+LocalDate.now().getDayOfMonth()+'/'+LocalDate.now().getYear();
//        }
        LocalDate localDate = LocalDate.parse(tanggal, formatter);
        LocalTime timeStart = LocalTime.parse(scheduleEvent.getScheduleTimeStartString());
        LocalTime timeEnd = LocalTime.parse(scheduleEvent.getScheduleTimeEndString());

        if (scheduleEvent.getMustMatch() != null){
            scheduleEvent.setMustMatch(StatusRecord.AKTIF);
        }else{
            scheduleEvent.setMustMatch(StatusRecord.NONAKTIF);
        }
        if (scheduleEvent.getUploadPicture() != null){
            scheduleEvent.setUploadPicture(StatusRecord.AKTIF);
        }else{
            scheduleEvent.setUploadPicture(StatusRecord.NONAKTIF);
        }

        scheduleEvent.setScheduleTimeStart(timeStart);
        scheduleEvent.setScheduleTimeEnd(timeEnd);
        scheduleEvent.setScheduleDate(localDate);
        scheduleEvent.setStatus(StatusRecord.AKTIF);
        scheduleEvent.setJenis(StatusRecord.TASK);
        scheduleEvent.setDateInsert(LocalDateTime.now());
        scheduleEvent.setCompanies(companies);
        scheduleEvent.setUserInsert(userEmployes.getId());
        scheduleEvent.setDateInsert(LocalDateTime.now());
        scheduleEventDao.save(scheduleEvent);

        redirectAttributes.addFlashAttribute("success", "Save Data Success");
        return "redirect:../templates";
    }

    @GetMapping("/task/template/edit")
    public String editTaskTemplatePage(@RequestParam(required = true) ScheduleEvent scheduleEvent , Model model, Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Employes userEmployes = employesDao.findByUser(user);
        model.addAttribute("userEmployes", userEmployes);
        model.addAttribute("scheduleTypeList", ScheduleType.values());
        model.addAttribute("scheduleEvent", scheduleEvent);

        return "templateTasks/templateEdit";
    }


    @PostMapping("/task/template/update")
    public String updateTaskTemplate(@ModelAttribute ScheduleEvent scheduleEvent,
                                   Authentication authentication,
                                   RedirectAttributes redirectAttributes) {

        User user = currentUserService.currentUser(authentication);
        Employes userEmployes = employesDao.findByUser(user);

        Companies companies = companiesDao.findByStatusAndId(StatusRecord.AKTIF, "b5c2e2a-b711-4037-8f60-1e1aa0415958");

        LocalDate localDate = LocalDate.now();
        if (scheduleEvent.getScheduleDateString().length() > 0) {
            String tanggal = scheduleEvent.getScheduleDateString();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
            localDate = LocalDate.parse(tanggal, formatter);
        }
        LocalTime timeStart = LocalTime.parse(scheduleEvent.getScheduleTimeStartString());
        LocalTime timeEnd = LocalTime.parse(scheduleEvent.getScheduleTimeEndString());

        if(scheduleEvent.getStatus() == null){
            scheduleEvent.setStatusAktif(StatusRecord.NONAKTIF);
            scheduleEvent.setStatus(StatusRecord.NONAKTIF);
        }
        if (scheduleEvent.getMustMatch() != null){
            scheduleEvent.setMustMatch(StatusRecord.AKTIF);
        }else{
            scheduleEvent.setMustMatch(StatusRecord.NONAKTIF);
        }
        if (scheduleEvent.getUploadPicture() != null){
            scheduleEvent.setUploadPicture(StatusRecord.AKTIF);
        }else{
            scheduleEvent.setUploadPicture(StatusRecord.NONAKTIF);
        }

        scheduleEvent.setScheduleTimeStart(timeStart);
        scheduleEvent.setScheduleTimeEnd(timeEnd);
        scheduleEvent.setScheduleDate(localDate);
        scheduleEvent.setUserUpdate(userEmployes.getId());
        scheduleEvent.setDateUpdate(LocalDateTime.now());
        scheduleEventDao.save(scheduleEvent);

        redirectAttributes.addFlashAttribute("success", "Save Data Success");
        return "redirect:../templates";
    }

    @GetMapping("/task/template/apply")
    public String applyPage(Model model,
                            @RequestParam(required = false) String search,
                            Authentication authentication,
                            @PageableDefault(size = 10)Pageable pageable) {

//        model.addAttribute("listEmployee", employesDao.findByStatusAndStatusAktifOrderByFullName(StatusRecord.AKTIF, "AKTIF", pageable));
        User user = currentUserService.currentUser(authentication);
        Employes userEmployes = employesDao.findByUser(user);
        model.addAttribute("userEmployes", userEmployes);

        if (StringUtils.hasText(search)) {
            model.addAttribute("search", search);
            model.addAttribute("listEmployee", employesDao.listKaryawanScheduleSearch(search, pageable));
        }else{
            model.addAttribute("listEmployee", employesDao.listKaryawanSchedule(pageable));
        }

        return "templateTasks/applyTask";
    }

}
