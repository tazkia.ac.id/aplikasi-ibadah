package id.ac.tazkia.ibadah.controller;

import id.ac.tazkia.ibadah.dao.config.UserDao;
import id.ac.tazkia.ibadah.dao.masterdata.EmployesDao;
import id.ac.tazkia.ibadah.dao.schedule.ScheduleEventDao;
import id.ac.tazkia.ibadah.dao.setting.EmployeeTaskScheduleDao;
import id.ac.tazkia.ibadah.dao.setting.EmployeeTasksDao;
import id.ac.tazkia.ibadah.dao.setting.PayrollPeriodeDao;
import id.ac.tazkia.ibadah.entity.StatusRecord;
import id.ac.tazkia.ibadah.entity.config.User;
import id.ac.tazkia.ibadah.entity.masterdata.Employes;
import id.ac.tazkia.ibadah.entity.schedule.ScheduleEvent;
import id.ac.tazkia.ibadah.entity.setting.EmployeeTasks;
import id.ac.tazkia.ibadah.entity.setting.EmployeeTasksSchedule;
import id.ac.tazkia.ibadah.entity.setting.PayrollPeriode;
import id.ac.tazkia.ibadah.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Controller
public class TaskController {

    @Autowired
    private ScheduleEventDao scheduleEventDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private EmployesDao employesDao;


    @Autowired
    private EmployeeTasksDao employeeTasksDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private PayrollPeriodeDao payrollPeriodeDao;

    @Autowired
    private EmployeeTaskScheduleDao employeeTaskScheduleDao;

    @GetMapping("/404")
    public String form404(){

        return "error";
    }

    @GetMapping("/home")
    public String homePage(Model model,
                           Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Employes userEmployes = employesDao.findByUser(user);
        model.addAttribute("userEmployes", userEmployes);
        List<StatusRecord> statusRecords = new ArrayList<>();
        statusRecords.add(StatusRecord.WAITING);
        statusRecords.add(StatusRecord.DONE);

        PayrollPeriode payrollPeriode = payrollPeriodeDao.findByStatus(StatusRecord.AKTIF);

        LocalDate tanggal = LocalDate.now();
        String bulanMulai = tanggal.getMonthValue()+"";
        if(bulanMulai.length() == 1 ){
            bulanMulai = "0" + bulanMulai;
        }
        String bulanSelesai = tanggal.plusMonths(1).getMonthValue()+"";
        if(bulanSelesai.length() == 1 ){
            bulanSelesai = "0" + bulanSelesai;
        }
        String tahunMulai = tanggal.getYear()+"";
        String tahunSelesai = tanggal.plusMonths(1).getYear()+"";

        String dayMulai = LocalDate.now().getDayOfMonth()+"";
        if(dayMulai.length() == 1 ){
            dayMulai = "0" + dayMulai;
        }

        String tanggalMulai = tahunMulai+'-'+bulanMulai+'-'+ dayMulai;
        String tanggalSelesai = tahunSelesai+'-'+bulanSelesai+'-'+payrollPeriode.getSampaiTanggal();

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDateMulai = LocalDate.parse(tanggalMulai, formatter);
        LocalDate localDateSelesai = LocalDate.parse(tanggalSelesai, formatter);

        String tanggalMulaiHitung=tanggal.toString();
        String tanggalSelesaiHitung=tanggal.toString();

        if(tanggal.getDayOfMonth() <= 20){
            String month1 = tanggal.minusMonths(1).getMonthValue()+"";
            String month2 = tanggal.getMonthValue()+"";
            if(month1.length() == 1){
                month1 = "0"+month1;
            }
            if(month2.length() == 1){
                month2 = "0"+month2;
            }
            if(tanggal.getMonthValue()==1) {
                tanggalMulaiHitung = tanggal.minusYears(1).getYear() + "-" + month1 + "-" + payrollPeriode.getDariTanggal();
                tanggalSelesaiHitung = tanggal.getYear() + "-" + month2 + "-" + payrollPeriode.getSampaiTanggal();
            }else{

                tanggalMulaiHitung = tanggal.getYear() + "-" + month1 + "-" + payrollPeriode.getDariTanggal();
                tanggalSelesaiHitung = tanggal.getYear() + "-" + month2 + "-" + payrollPeriode.getSampaiTanggal();
            }
        }else{
            String month1 = tanggal.getMonthValue()+"";
            String month2 = tanggal.plusMonths(1).getMonthValue()+"";
            if(month1.length() == 1){
                month1 = "0"+month1;
            }
            if(month2.length() == 1){
                month2 = "0"+month2;
            }
            if(tanggal.getMonthValue()==12) {
                tanggalMulaiHitung = tanggal.getYear() + "-" + month1 + "-" + payrollPeriode.getDariTanggal();
                tanggalSelesaiHitung = tanggal.plusYears(1).getYear() + "-01-" + payrollPeriode.getSampaiTanggal();
            }else{
                tanggalMulaiHitung = tanggal.getYear() + "-" + month1 + "-" + payrollPeriode.getDariTanggal();
                tanggalSelesaiHitung = tanggal.getYear() + "-" + month2+ "-" + payrollPeriode.getSampaiTanggal();
            }
        }


        LocalDate localDateMulaiHitung = LocalDate.parse(tanggalMulaiHitung, formatter);
        LocalDate localDateSelesaiHitung = LocalDate.parse(tanggalSelesaiHitung, formatter);

        List<EmployeeTasksSchedule> employeeTasksSchedule = employeeTaskScheduleDao.findByEmployeeTasksEmployesAndEmployeeTasksScheduleEventJenisNotAndStatusNotAndEmployeeTasksStatusAndDateTasksOrderByEmployeeTasksScheduleEventScheduleTimeStart(userEmployes,StatusRecord.EVENT,  StatusRecord.HAPUS , StatusRecord.AKTIF, LocalDate.now());

//        List<EmployeeTasksSchedule> employeeTasksSchedule = employeeTaskScheduleDao.findByEmployeeTasksEmployesAndEmployeeTasksScheduleEventJenisNotAndStatusInAndEmployeeTasksStatusAndDateTasksOrderByEmployeeTasksScheduleEventScheduleTimeStart(userEmployes,StatusRecord.EVENT,  statusRecords , StatusRecord.AKTIF, LocalDate.now());
        model.addAttribute("listEmployeeTasks", employeeTasksSchedule);
        model.addAttribute("listScheduleEvent", scheduleEventDao.cariEventEmployee(LocalDate.now(),userEmployes.getId(), userEmployes.getEmployeeClass().toString()));
        Object totalTasksNominal = employeeTaskScheduleDao.jumlahNominalPegawai(userEmployes.getId(), localDateMulaiHitung, localDateSelesaiHitung);
        model.addAttribute("totalNominal", totalTasksNominal);
        model.addAttribute("dataPrestasi", employeeTaskScheduleDao.dataPrestasi(userEmployes.getId()));
        model.addAttribute("tanggalMulai", localDateMulaiHitung);
        model.addAttribute("tanggalSelesai", localDateSelesaiHitung);


        return "tasks/index";
    }

    @GetMapping("/")
    public String formAwal(){
        return "redirect:/home";
    }

    @GetMapping("/task/all")
    public String taskPage(Model model,
                           Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Employes userEmployes = employesDao.findByUser(user);
        model.addAttribute("userEmployes", userEmployes);
        PayrollPeriode payrollPeriode = payrollPeriodeDao.findByStatus(StatusRecord.AKTIF);

        LocalDate tanggal = LocalDate.now();
        String bulanMulai = tanggal.getMonthValue()+"";
        if(bulanMulai.length() == 1 ){
            bulanMulai = "0" + bulanMulai;
        }
        String bulanSelesai = tanggal.plusMonths(1).getMonthValue()+"";
        if(bulanSelesai.length() == 1 ){
            bulanSelesai = "0" + bulanSelesai;
        }
        String tahunMulai = tanggal.getYear()+"";
        String tahunSelesai = tanggal.plusMonths(1).getYear()+"";

        String dayMulai = LocalDate.now().getDayOfMonth()+"";
        if(dayMulai.length() == 1 ){
            dayMulai = "0" + dayMulai;
        }

        String tanggalMulai = tahunMulai+'-'+bulanMulai+'-'+ dayMulai;
        String tanggalSelesai = tahunSelesai+'-'+bulanSelesai+'-'+payrollPeriode.getSampaiTanggal();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDateMulai = LocalDate.parse(tanggalMulai, formatter);
        LocalDate localDateSelesai = LocalDate.parse(tanggalSelesai, formatter);

        String tanggalMulaiHitung=tanggal.toString();
        String tanggalSelesaiHitung=tanggal.toString();
        if(tanggal.getDayOfMonth() <= 20){
            String month1 = tanggal.minusMonths(1).getMonthValue()+"";
            String month2 = tanggal.getMonthValue()+"";
            if(month1.length() == 1){
                month1 = "0"+month1;
            }
            if(month2.length() == 1){
                month2 = "0"+month2;
            }
            if(tanggal.getMonthValue()==1) {
                tanggalMulaiHitung = tanggal.minusYears(1).getYear() + "-" + month1 + "-" + payrollPeriode.getDariTanggal();
                tanggalSelesaiHitung = tanggal.getYear() + "-" + month2 + "-" + payrollPeriode.getSampaiTanggal();
            }else{

                tanggalMulaiHitung = tanggal.getYear() + "-" + month1 + "-" + payrollPeriode.getDariTanggal();
                tanggalSelesaiHitung = tanggal.getYear() + "-" + month2 + "-" + payrollPeriode.getSampaiTanggal();
            }
        }else{
            String month1 = tanggal.getMonthValue()+"";
            String month2 = tanggal.plusMonths(1).getMonthValue()+"";
            if(month1.length() == 1){
                month1 = "0"+month1;
            }
            if(month2.length() == 1){
                month2 = "0"+month2;
            }
            if(tanggal.getMonthValue()==12) {
                tanggalMulaiHitung = tanggal.getYear() + "-" + month1 + "-" + payrollPeriode.getDariTanggal();
                tanggalSelesaiHitung = tanggal.plusYears(1).getYear() + "-01-" + payrollPeriode.getSampaiTanggal();
            }else{
                tanggalMulaiHitung = tanggal.getYear() + "-" + month1 + "-" + payrollPeriode.getDariTanggal();
                tanggalSelesaiHitung = tanggal.getYear() + "-" + month2+ "-" + payrollPeriode.getSampaiTanggal();
            }
        }

        LocalDate localDateMulaiHitung = LocalDate.parse(tanggalMulaiHitung, formatter);
        LocalDate localDateSelesaiHitung = LocalDate.parse(tanggalSelesaiHitung, formatter);

        Object totalTasksNominal = employeeTaskScheduleDao.jumlahNominalPegawai(userEmployes.getId(), localDateMulaiHitung, localDateSelesaiHitung);
        model.addAttribute("totalNominal", totalTasksNominal);
        model.addAttribute("tanggalMulai", localDateMulaiHitung);
        model.addAttribute("tanggalSelesai", localDateSelesaiHitung);
        model.addAttribute("dataPrestasi", employeeTaskScheduleDao.dataPrestasi(userEmployes.getId()));

        return "tasks/all";
    }

    @GetMapping("/task/history")
    public String taskHistoryPage(Model model,
                                  Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Employes userEmployes = employesDao.findByUser(user);
        model.addAttribute("userEmployes", userEmployes);
        PayrollPeriode payrollPeriode = payrollPeriodeDao.findByStatus(StatusRecord.AKTIF);

        LocalDate tanggal = LocalDate.now();
        String bulanMulai = tanggal.getMonthValue()+"";
        String tanggalMulaiHitung=tanggal.toString();
        String tanggalSelesaiHitung=tanggal.toString();
        if(tanggal.getDayOfMonth() <= 20){
            String month1 = tanggal.minusMonths(1).getMonthValue()+"";
            String month2 = tanggal.getMonthValue()+"";
            if(month1.length() == 1){
                month1 = "0"+month1;
            }
            if(month2.length() == 1){
                month2 = "0"+month2;
            }
            if(tanggal.getMonthValue()==1) {
                tanggalMulaiHitung = tanggal.minusYears(1).getYear() + "-" + month1 + "-" + payrollPeriode.getDariTanggal();
                tanggalSelesaiHitung = tanggal.getYear() + "-" + month2 + "-" + payrollPeriode.getSampaiTanggal();
            }else{

                tanggalMulaiHitung = tanggal.getYear() + "-" + month1 + "-" + payrollPeriode.getDariTanggal();
                tanggalSelesaiHitung = tanggal.getYear() + "-" + month2 + "-" + payrollPeriode.getSampaiTanggal();
            }
        }else{
            String month1 = tanggal.getMonthValue()+"";
            String month2 = tanggal.plusMonths(1).getMonthValue()+"";
            if(month1.length() == 1){
                month1 = "0"+month1;
            }
            if(month2.length() == 1){
                month2 = "0"+month2;
            }
            if(tanggal.getMonthValue()==12) {
                tanggalMulaiHitung = tanggal.getYear() + "-" + month1 + "-" + payrollPeriode.getDariTanggal();
                tanggalSelesaiHitung = tanggal.plusYears(1).getYear() + "-01-" + payrollPeriode.getSampaiTanggal();
            }else{
                tanggalMulaiHitung = tanggal.getYear() + "-" + month1 + "-" + payrollPeriode.getDariTanggal();
                tanggalSelesaiHitung = tanggal.getYear() + "-" + month2+ "-" + payrollPeriode.getSampaiTanggal();
            }
        }

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDateMulaiHitung = LocalDate.parse(tanggalMulaiHitung, formatter);
        LocalDate localDateSelesaiHitung = LocalDate.parse(tanggalSelesaiHitung, formatter);

        Object totalTasksNominal = employeeTaskScheduleDao.jumlahNominalPegawai(userEmployes.getId(), localDateMulaiHitung, localDateSelesaiHitung);
        model.addAttribute("totalNominal", totalTasksNominal);
        model.addAttribute("tanggalMulai", localDateMulaiHitung);
        model.addAttribute("tanggalSelesai", localDateSelesaiHitung);
        model.addAttribute("dataPrestasi", employeeTaskScheduleDao.dataPrestasi(userEmployes.getId()));
        model.addAttribute("tanggalHistory", scheduleEventDao.generateTanggal(localDateMulaiHitung, localDateSelesaiHitung));

        return "tasks/history";
    }

    @GetMapping("/task/employee")
    public String taskEmployee(@RequestParam(required = true) String employes,
                               @RequestParam(required = false) String search,
                               @RequestParam(required = false) String searchTasks,
                               Authentication authentication,
                               Model model,
                               Pageable pageable) {
        User user = currentUserService.currentUser(authentication);
        Employes userEmployes = employesDao.findByUser(user);
        model.addAttribute("userEmployes", userEmployes);
        Employes employes1 = employesDao.findByStatusAndId(StatusRecord.AKTIF, employes);
        if (StringUtils.hasText(searchTasks)) {
            model.addAttribute("searchTasks", searchTasks);
            model.addAttribute("listTaskEmployes", employeeTasksDao.findByStatusAndEmployesAndScheduleEventTaskContainingIgnoreCase(StatusRecord.AKTIF, employes1, searchTasks, pageable));
        }else{
            model.addAttribute("listTaskEmployes", employeeTasksDao.findByStatusAndEmployes(StatusRecord.AKTIF, employes1, pageable));
        }

        if (StringUtils.hasText(search)) {
            model.addAttribute("search", search);
            model.addAttribute("listScheduleEvent", scheduleEventDao.findByStatusAndJenisNotAndTaskContainingIgnoreCaseOrderByScheduleName(StatusRecord.AKTIF, StatusRecord.EVENT, search));
        }else{
            model.addAttribute("listScheduleEvent", scheduleEventDao.findByStatusAndJenisNotOrderByScheduleName(StatusRecord.AKTIF, StatusRecord.EVENT));
        }
        model.addAttribute("employes", employes1);

        return "tasks/employeeTask";
    }

    @PostMapping("/task/employee/save")
    public String taskEmployeeSave(@RequestParam(required = true)String employes,
                                   @RequestParam(required = false) List<ScheduleEvent> scheduleEvent,
                                   Authentication authentication,
                                   RedirectAttributes redirectAttributes){

        User user = currentUserService.currentUser(authentication);
        Employes userEmployes = employesDao.findByUser(user);
        Employes employes1 = employesDao.findByStatusAndId(StatusRecord.AKTIF, employes);

        if(scheduleEvent == null){
            redirectAttributes.addFlashAttribute("gagal", "Add Template Task Failed");
            return "redirect:../employee?employes="+ employes;
        }else{
            for(ScheduleEvent scheduleEvent1 : scheduleEvent){
                EmployeeTasks employeeTasks = new EmployeeTasks();
                employeeTasks.setId(UUID.randomUUID().toString());
                employeeTasks.setEmployes(employes1);
                employeeTasks.setScheduleEvent(scheduleEvent1);
                employeeTasks.setStatus(StatusRecord.AKTIF);
                employeeTasks.setTanggalInsert(LocalDateTime.now());
                employeeTasks.setUserInsert(userEmployes.getId());
                employeeTasks.setTanggalInsert(LocalDateTime.now());
                employeeTasksDao.save(employeeTasks);
            }
        }

        redirectAttributes.addFlashAttribute("success", "Template Task Successfully Added");
        return "redirect:../employee?employes="+ employes;
    }

    @PostMapping("/task/employee/delete")
    public String taskEmployeeSave(@RequestParam(required = true)String employeeTasks,
                                   Authentication authentication,
                                   RedirectAttributes redirectAttributes){

        User user = currentUserService.currentUser(authentication);
        Employes userEmployes = employesDao.findByUser(user);

        EmployeeTasks employeeTasks1 = employeeTasksDao.findByStatusAndId(StatusRecord.AKTIF, employeeTasks);
        Employes employes = employeeTasks1.getEmployes();

        if(employeeTasks1 == null){
            redirectAttributes.addFlashAttribute("gagal", "Delete Template Task Failed");
            return "redirect:../employee?employes="+ employes.getId();
        }else{
            employeeTasks1.setStatus(StatusRecord.HAPUS);
            employeeTasks1.setTanggalDelete(LocalDateTime.now());
            employeeTasks1.setUserDelete(userEmployes.getId());
            employeeTasks1.setTanggalDelete(LocalDateTime.now());
            employeeTasksDao.save(employeeTasks1);
        }

        redirectAttributes.addFlashAttribute("success", "Template Task Successfully Deleted");
        return "redirect:../employee?employes="+ employes.getId();
    }

    @GetMapping("/task/add")
    public String taskAddPage(){return "tasks/taskAdd";}

    @GetMapping("task/edit")
    public String taskEditPage(@RequestParam(name ="id") Integer id ){return "tasks/taskEdit";}
}
