package id.ac.tazkia.ibadah.controller;


import id.ac.tazkia.ibadah.dao.config.UserDao;
import id.ac.tazkia.ibadah.dao.masterdata.EmployesDao;
import id.ac.tazkia.ibadah.entity.EmployeeClass;
import id.ac.tazkia.ibadah.entity.StatusRecord;
import id.ac.tazkia.ibadah.entity.config.User;
import id.ac.tazkia.ibadah.entity.masterdata.Employes;
import id.ac.tazkia.ibadah.services.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
public class EmployeeGroupController {

    @Autowired
    private UserDao userDao;

    @Autowired
    private EmployesDao employesDao;

    @Autowired
    CurrentUserService currentUserService;
    @GetMapping("/employee-group")
    public String employeeGroupPage(Model model,
                                    @RequestParam(required = false) EmployeeClass employeClass,
                                    @RequestParam(required = false) String search,
                                    @PageableDefault(size = 10)Pageable pageable,
                                    Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Employes userEmployes = employesDao.findByUser(user);
        model.addAttribute("userEmployes", userEmployes);


        if(employeClass == null) {
            if (StringUtils.hasText(search)) {
                model.addAttribute("search", search);
                model.addAttribute("listEmployes", employesDao.findByStatusAndStatusAktifAndFullNameContainingIgnoreCaseOrderByFullName(StatusRecord.AKTIF, StatusRecord.AKTIF, search, pageable));
            } else {
                model.addAttribute("listEmployes", employesDao.findByStatusAndStatusAktifOrderByFullName(StatusRecord.AKTIF, StatusRecord.AKTIF, pageable));
            }
        }else{
            model.addAttribute("employeeClassSelected", employeClass);
            if (StringUtils.hasText(search)) {
                model.addAttribute("search", search);
                model.addAttribute("listEmployes", employesDao.findByStatusAndStatusAktifAndEmployeeClassAndFullNameContainingIgnoreCaseOrderByFullName(StatusRecord.AKTIF, StatusRecord.AKTIF, employeClass, search, pageable));
            } else {
                model.addAttribute("listEmployes", employesDao.findByStatusAndStatusAktifAndEmployeeClassOrderByFullName(StatusRecord.AKTIF, StatusRecord.AKTIF, employeClass, pageable));
            }
        }
        model.addAttribute("listEmployeeClass", EmployeeClass.values());


        return "employee/employeeGroup";
    }


    @PostMapping("/employee-group/save")
    public String saveEmployeeGroup(@RequestParam(required = false) List<Employes> id,
                                    @RequestParam(required = false) EmployeeClass employeeClass,
                                    Authentication authentication,
                                    RedirectAttributes redirectAttributes){

        User user = currentUserService.currentUser(authentication);
        Employes userEmployes = employesDao.findByUser(user);

        System.out.println("Employee : "+ id);
        System.out.println("Class : "+ employeeClass);
        if(id != null){
            for(Employes employes : id){
                employes.setEmployeeClass(employeeClass);
                employesDao.save(employes);
            }
            redirectAttributes.addFlashAttribute("success", "Employee Class Successfully Update");
        }else{
            redirectAttributes.addFlashAttribute("gagal", "None of the data selected");
        }

        return "redirect:../employee-group";
    }
}
