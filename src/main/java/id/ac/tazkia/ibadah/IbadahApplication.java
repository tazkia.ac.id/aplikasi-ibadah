package id.ac.tazkia.ibadah;

import nz.net.ultraq.thymeleaf.LayoutDialect;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.thymeleaf.dialect.springdata.SpringDataDialect;

@SpringBootApplication
public class IbadahApplication {

	public static void main(String[] args) {
		SpringApplication.run(IbadahApplication.class, args);
	}
	@Bean
	public SpringDataDialect springDataDialect() {
		return new SpringDataDialect();
	}


	@Bean
	public LayoutDialect layoutDialect() {
		return new LayoutDialect();
	}


}
