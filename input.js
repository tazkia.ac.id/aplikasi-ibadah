import 'flowbite';
import Datepicker from 'flowbite-datepicker/Datepicker';
import DateRangePicker from 'flowbite-datepicker/DateRangePicker';
import el from "flowbite-datepicker/locales/el";


const path = window.location.href;
const navLink = document.querySelectorAll('.horizontalNavMenu a')
navLink.forEach(function (e){
    if (e.href === path) {
        e.classList.remove('text-gray-900' , 'bg-gray-100')
        e.classList.add('text-white' , 'bg-blue-700');
    }
})
if (document.getElementById('dateRangePicker')){
    const dateRangePickerEl = document.getElementById('dateRangePicker');
    new DateRangePicker(dateRangePickerEl, {
        autohide : true,
    });
}

if (document.getElementById('date')){
    const datepickerEl = document.getElementById('date');
    new Datepicker(datepickerEl, {
        autohide : true,
    });
}

if (document.getElementById('repeat')){
    const formSelect = document.getElementById('repeat')
    const formDate = document.getElementById('onceRepeat')
    const formDays = document.getElementById('customRepeat')

    formSelect.addEventListener("change", toggleForm );
    function toggleForm(){
        if (formSelect.value === 'Custom') {
            formDays.classList.remove('hidden');
            formDate.classList.add('hidden');
        } else if (formSelect.value === 'Once') {
            formDate.classList.remove('hidden');
            formDays.classList.add('hidden');
        } else {
            formDate.classList.add('hidden');
            formDays.classList.add('hidden');
        }
    }

    toggleForm();
}

if (document.getElementById('toggle-reword')){
    const toggleRewordBtn = document.getElementById('toggle-reword')
    const reword = document.getElementById('reword')
    const hintText = document.getElementById('hintText')

    toggleRewordBtn.addEventListener("click", function () {
        reword.classList.toggle('hidden');
        hintText.classList.toggle('hidden');
    });
}